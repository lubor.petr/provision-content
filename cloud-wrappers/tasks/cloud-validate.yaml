---
Name: "cloud-validate"
Description: "Check for minimum provisioning values"
Documentation: |
  Used as a validate that the right params have been set for cloud
  scenarios and provide operator friendly feedback in the Machine.Description

  Maintainer Notes: Remember to synchronize with the `cloud/provider` enum!

RequiredParams:
  - cloud/provider
Templates:
  - Name: "cloud-validate"
    Contents: |
      #!/bin/bash
      # RackN Copyright 2020

      {{template "setup.tmpl" .}}

      {{ $cloud := .Param "cloud/provider" }}

      function verified() {
        param=$1
        result=$2
        if [[ "$result" == "true" ]]; then
          echo "Verified for {{$cloud}}: $param is set"
        else
          echo "ERROR: $param is REQUIRED for {{$cloud}}"
          exit 1
        fi
      }

      echo "We have cloud/provider {{ $cloud }}!  Do we have the right credentials?"

      {{ if eq "linode" $cloud }}
        verified "linode/token" {{.ParamExists "linode/token"}}
        exit 0
      {{ end }}

      {{ if eq "aws" $cloud }}
        verified "aws/secret-key" {{.ParamExists "aws/secret-key"}}
        verified "aws/access-key-id" {{.ParamExists "aws/access-key-id"}}
        exit 0
      {{ end }}

      {{ if eq "google" $cloud }}
        verified "google/credential" {{.ParamExists "google/credential"}}
        verified "google/project-id" {{.ParamExists "google/project-id"}}
        exit 0
      {{ end }}

      {{ if eq "digitalocean" $cloud }}
        verified "digitalocean/token" {{.ParamExists "digitalocean/token"}}
        exit 0
      {{ end }}

      {{ if eq "azure" $cloud }}
        verified "azure/subscription_id" {{.ParamExists "azure/subscription_id"}}
        verified "azure/app_id" {{.ParamExists "azure/app_id"}}
        verified "azure/password" {{.ParamExists "azure/password"}}
        verified "azure/tenant" {{.ParamExists "azure/tenant"}}
        exit 0
      {{ end }}

      {{ if eq "pnap" $cloud }}
        verified "pnap/client-secret" {{.ParamExists "pnap/client-secret"}}
        verified "pnap/client-id" {{.ParamExists "pnap/client-id"}}
        exit 0
      {{ end }}

      {{ if eq "oracle" $cloud }}
        verified "oracle/user-ocid" {{.ParamExists "oracle/user-ocid"}}
        verified "oracle/tenancy-ocid" {{.ParamExists "oracle/tenancy-ocid"}}
        verified "oracle/fingerprint" {{.ParamExists "oracle/fingerprint"}}
        verified "oracle/private-key" {{.ParamExists "oracle/private-key"}}
        verified "oracle/subnet-id" {{.ParamExists "oracle/subnet-id"}}
        verified "oracle/compartment-id" {{.ParamExists "oracle/compartment-id"}}
        echo "SPECIAL REQUIREMENT FOR ORACLE - copy access keys to local system"
        tee /tmp/oracle_private.pem >/dev/null << EOF
      {{.Param "oracle/private-key" | replace "KEY-----" "KEY-----\n" | replace "-----END" "\n-----END" }}
      EOF
        chmod 600 /tmp/oracle_private.pem
        exit 0
      {{ end }}

      echo "WARNING: no validation for cloud/provider {{$cloud}}"
      exit 0
Meta:
  type: "discover"
  icon: "check"
  color: "blue"
  title: "RackN Content"
  feature-flags: "sane-exit-codes"
