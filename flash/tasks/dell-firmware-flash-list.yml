---
Name: dell-firmware-flash-list
Description: Dell Firmware Flash From List
RequiredParams:
  - skip-flash
  - flash-list
Templates:
  - Name: pendingJobs
    Path: "pendingJobs.awk"
    Contents: |
      BEGIN { FS = "="; OFS = " | " }
      /Job ID/ { jobId = $2 }
      /Job Name/ { jobName = $2 }
      /Status/ {
          status = $2
          if (jobName !~ /^Firmware: Update/ || status ~ /(Completed|Failed)/) {
              next
          }
          print jobId, jobName, status
      }
  - Name: dell-python-script
    Path: dell-redfish.py
    ID: DeviceFirmwareSimpleUpdateCheckVersionREDFISH.py.tmpl
  - Name: dell-firmware-flash-list
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}
      {{ template "set-proxy-servers.sh.tmpl" . }}

      function clean_curl() {
          local _src=$1
          local _dest=$2

          echo "Downloading $_src"
          set -e
          curl -L -f -k -o $_dest $_src
          set +e
          if [[ $? != 0 ]] ; then
              xiterr 1 "Failed to download $_dest from $_src"
          fi
      }

      skip_flash="{{.Param "skip-flash"}}"
      if [[ $skip_flash = true ]]; then
          echo "Skipping all flash operations due to skip-flash being true"
          exit 0
      fi

      if ! grep -q 'sledgehammer\.iso' /proc/cmdline; then
          echo "System not in Sledgehammer, exiting"
          exit 0
      fi

      drpcli machines remove $RS_UUID param dell-pending-flash-jobs &> /dev/null || :
      pj="$(awk -f pendingJobs.awk < <(/opt/dell/srvadmin/sbin/racadm jobqueue view) | sort)"
            if [[ $pj ]]; then
          if [[ $pj == '{{ .Param "dell-pending-flash-jobs" }}' ]]; then
              echo "Pending server config jobs did not resolve over a reboot.  Manual intervention required."
              echo "$pj"
              exit 1
          fi
         drpcli machines set $RS_UUID param dell-pending-flash-jobs to "$pj" &>/dev/null
         echo "Job queue has pending, running, or scheduled server config jobs - reboot to see if they clear"
         echo "$pj"
         exit_incomplete_reboot
      fi
      want_reboot=no
      failed=no
      hpe=no
      {{ range $index, $elem := .Param "flash-list" }}
        {{ if not (has $elem.File ($.Param "flash-list-check-list")) }}
        FILENAME="file.{{$index}}"
        {{ if $elem.Force }}
        FORCE="-f"
        {{ else }}
        FORCE=
        {{ end }}
        {{ if $.Param "flash-list-force" }}
        FORCE="-f"
        {{ end }}

        {{ if hasPrefix "http" $elem.File }}
        clean_curl "{{$elem.File}}" "$FILENAME"
        {{ else }}
        clean_curl "{{$.ProvisionerURL}}/{{$elem.File}}" "$FILENAME"
        {{ end }}

        lfailed=no
        {{ if eq $elem.Type "bin" }}
        chmod +x $FILENAME
        set -o pipefail
        set +e
        ./$FILENAME -q ${FORCE} |& tee $FILENAME.log
        case $? in
          0)
             echo "$FILENAME succeeded";;
          1)
             # If the system is up to date, this is the return code and
             # the log will have the message "No Applicable Updates Available"
             lfailed=yes
             fgrep -q "No Applicable Updates Available" $FILENAME.log && lfailed=no
             ;;
          2)
             echo "$FILENAME wants a reboot"
             want_reboot=yes;;
          3)
             echo "Soft error for $FILENAME"
             echo "Likely at the exact level already.";;
          4)
             echo "Hard error for $FILENAME"
             lfailed=yes;;
          5)
             echo "Unquailifed update for $FILENAME"
             echo "Either missing or not applicable - skipping";;
          6)
             echo "Reboot started!! for $FILENAME"
             want_reboot=yes;;
          9)
             echo "RPM verify failed for $FILENAME"
             lfailed=yes;;
          13)
             echo "Dependency failure, but success!";;
          14)
             echo "Dependency failure, but reboot wanted"
             want_reboot=yes;;
        esac
        set +o pipefail
        set -e
        {{ else }}
        {{ if eq $elem.Type "exe" }}

        echo "We should do better than this"
        yum -y install python3-requests || :

        echo "Setting up local link"
        racadm=/opt/dell/srvadmin/bin/idracadm7
        $racadm set idrac.os-bmc.AdminState Enabled || :
        COUNT=0
        while !  ip link | grep -q idrac ; do
          echo "no idrac - yet"
          sleep 30
          if (($COUNT > 10)) ; then
              echo "Failed to get idrac interface"
              exit 1
          fi
          COUNT=$((COUNT+1))
        done
        ip link set dev idrac up || :
        ip a a 169.254.1.2/24 dev idrac || :

        chmod +x ./dell-redfish.py
        mv $FILENAME $FILENAME.EXE
        set -o pipefail
        set +e
        ./dell-redfish.py --image=$FILENAME.EXE --location=. -ip 169.254.1.1 -u {{$.Param "ipmi/username"}} -p "{{$.Param "ipmi/password"}}" |& tee $FILENAME.log
        case $? in
          0)
             echo "$FILENAME succeeded";;
          1)
             # If the system is up to date, this is the return code and
             # the log will have the message "No Applicable Updates Available"
             lfailed=yes
             fgrep -q "No Applicable Updates Available" $FILENAME.log && lfailed=no
             ;;
          2)
             echo "$FILENAME wants a reboot"
             want_reboot=yes;;
          3)
             echo "Soft error for $FILENAME"
             echo "Likely at the exact level already.";;
          4)
             echo "Hard error for $FILENAME"
             lfailed=yes;;
          5)
             echo "Unquailifed update for $FILENAME"
             echo "Either missing or not applicable - skipping";;
          6)
             echo "Reboot started!! for $FILENAME"
             want_reboot=yes;;
          9)
             echo "RPM verify failed for $FILENAME"
             lfailed=yes;;
          13)
             echo "Dependency failure, but success!";;
          14)
             echo "Dependency failure, but reboot wanted"
             want_reboot=yes;;
        esac
        set -e
        set +o pipefail

        echo "Disable idrac nic"
        $racadm set idrac.os-bmc.AdminState Disabled || :

        {{ end }}
        {{ end }}

        if [[ $lfailed = no ]] ; then
          drpcli machines get $RS_UUID param flash-list-check-list | jq ' (.+ ["{{$elem.File}}"] | unique)' | drpcli machines set $RS_UUID param flash-list-check-list to -
        fi
        if [[ $lfailed = yes ]] ; then
          failed=yes
        fi
        {{ else }}
        echo "Image {{$elem.File}} is already installed.  To reinstall, clear the 'flash-list-check-list'"
        {{ end }}
      {{ end }}

      if [[ $want_reboot = yes ]]; then
          echo "Need reboot - start rebooting"
          exit_incomplete_reboot
      fi

      if [[ $failed = yes ]]; then
          echo "Something failed - error"
          exit 1
      fi

      {{ template "flash-list-installed.sh.tmpl" . }}

      echo "Nothing else to do and complete"
      drpcli machines set "$RS_UUID" param skip-flash to true
      exit 0
