#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# quick and dirty setup of Packer on a system
set -e

PACKER_VERSION={{ .Param "packer-builder/packer-version" }}

function setup_ubuntu_22() {
  install_lookup \
    ansible \
    curl \
    genisoimage \
    git \
    libarchive-tools \
    libfuse2 \
    make \
    unzip \
    wget

}
# packer UEFI builds require OVMF firmware in a specific location matching ubuntu
function download_ovmf() {
  echo "Downloading and installing OVMF firmware"
  cd /etc/yum.repos.d/
  curl -O https://www.kraxel.org/repos/firmware.repo
  yum install edk2.git-ovmf-x64 -y

  echo "Linking OVMF firmware to the expected location for packer UEFI builds"
  ln -sf /usr/share/edk2.git/ovmf-x64/OVMF_CODE-pure-efi.fd /usr/share/OVMF/OVMF_CODE.fd
  ln -sf /usr/share/edk2.git/ovmf-x64/OVMF_VARS-pure-efi.fd /usr/share/OVMF/OVMF_VARS.fd
}
function setup_rhelish8() {
  install_lookup \
    ansible \
    bsdtar \
    curl \
    edk2-ovmf \
    genisoimage \
    git \
    libselinux \
    make \
    unzip \
    wget

  download_ovmf
}
function setup_rhelish9() {
  install_lookup \
    ansible \
    bsdtar \
    curl \
    edk2-ovmf \
    genisoimage \
    git \
    libxcrypt-compat \
    libselinux \
    make \
    unzip \
    wget

  download_ovmf
}

case $osfamily in
ubuntu)
  case $osversion in
    22.04)
      setup_ubuntu_22
      ;;
    *)
      xiterr 1 "Unsupported Ubuntu version '$osversion'."
      ;;
  esac
  ;;
almalinux|centos|redhat|rhel|fedora|rocky)
  case $osversion in
    8.*)
      setup_rhelish8
      ;;
    9.*)
      setup_rhelish9
      ;;
    *)
      xiterr 1 "Unsupported RHEL version '$osversion'."
      ;;
  esac
  ;;
*)
  xiterr 1 "Unsupported OS family '$osfamily'."
  ;;
esac


echo ""
echo "Setup vagrant/packer/packer-provisioner-windows-update-linux:"
echo ""
cd /usr/local/bin/

echo "Downloading packer ${PACKER_VERSION}"
curl -s https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip | zcat > packer && chmod 755 packer

echo "Sanity check the binaries"
echo -n "Packer:"
./packer --version

exit 0
