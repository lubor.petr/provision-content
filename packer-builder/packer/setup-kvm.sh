#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# quick and dirty setup of KVM to be used by packer

set -e
function setup_ubuntu_22() {
  install_lookup qemu-kvm virt-manager libvirt-daemon-system virtinst libvirt-clients bridge-utils

  systemctl enable --now libvirtd
  systemctl start libvirtd
}

function setup_rhelish8() {
  install_lookup epel-release
  install_lookup @virt virt-top libguestfs-tools virt-install bridge-utils libvirt-devel

  # packer looks for qemu-system-x86_64, but it has been renamed?
  ln -fs /usr/libexec/qemu-kvm /bin/qemu-system-x86_64
}

function setup_rhelish9() {
  install_lookup epel-release
  install_lookup qemu-kvm libvirt virt-manager virt-install bridge-utils virt-top libguestfs-tools bridge-utils virt-viewer

  # packer looks for qemu-system-x86_64, but it has been renamed?
  ln -fs /usr/libexec/qemu-kvm /bin/qemu-system-x86_64
}

main() {

  case $osfamily in
  ubuntu)
    case $osversion in
      22.04)
        setup_ubuntu_22
        ;;
      *)
        xiterr 1 "Unsupported Ubuntu version '$osversion'."
        ;;
    esac
    ;;
  almalinux|centos|redhat|rhel|fedora|rocky)
    case $osversion in
      8.*)
        setup_rhelish8
        ;;
      9.*)
        setup_rhelish9
        ;;
      *)
        xiterr 1 "Unsupported RHEL version '$osversion'."
        ;;
    esac
    ;;
  *)
    xiterr 1 "Unsupported OS family '$osfamily'."
    ;;
  esac


  # unfortunately "warn" level errors (non-fatal) are reported as with Exit code 1
  # which will blow up our automation - for now we're capturing this output for
  # informational /logging purposes
  virt-host-validate || true
  kvm-ok || true

}

main $*

echo ""
echo "COMPLETE"
echo "Example VM launch to test libvirt (PXE boot):"
echo ""
echo "virt-install --name=foobar --ram=4096 --cpu host --hvm --vcpus=2 --os-type=linux --disk /var/lib/libvirt/images/disk.qcow2,size=20,bus=ide --network bridge=virbr0,model=e1000 --pxe --graphics vnc,listen=127.0.0.1,password=foobar --check all=off"
echo ""



exit $?
