#!/usr/bin/env bash
# upload the post-processor-convert artifact to DRP for image deploy

# our post-processor-convert.sh script should produce the img.xz
# artifact as {{BuildName}}.img.xz - our

set -e
function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }
build_dir=$1
complete_dir=$2

# manifest comes from the packer post processor.
[[ -f "manifest.json" ]] || xiterr 1 "manifest.json not found."
# get the name of the image file from the manifest
build_path=$( cat manifest.json | jq -r '.builds[0].files[0].name')
build_name=$(basename $build_path)
[[ -n $build_name ]] || xiterr 1 "Build name not set."

IMG=$complete_dir/${build_name}.img.xz

[[ -d $complete_dir ]] || xiterr 1 "Complete directory '$complete_dir' not found."

if [[ ! -r "$IMG" ]]
then
  echo "FATAL: Unable to read '$IMG' image file.  Where is my image to upload?"
  echo "       NOT EXITING WITH ERROR CODE - as that will nuke Packer and all"
  echo "       build artifacts"
  exit 0
fi

echo "Starting image upload to DRP Endpoint ... "

if drpcli info check > /dev/null 2>&1
then
  echo "Successfully connected to DRP Endpoint ... "
else
  echo "FATAL: Unable to connect to DRP Entpoint.  You must specify the"
  echo "       DRP Endpoint via the RS_* shell environment variables, like:"
  echo ""
  echo "       export RS_ENDPOINT=https://10.10.10.10:8092"
  echo "       export RS_USERNAME=rocketskates"
  echo "       export RS_PASSWORD=r0cketsk8ts"
  echo ""
  echo "You may also specify them in the $HOME/.drpclirc file."
  echo ""
  echo "NOT EXITING WITH ERROR CODE - as that will nuke Packer and all"
  echo "build artifacts"
  exit 0
fi

echo ""
echo "IF this process fails, the script will still exit 0"
echo ""
echo "Beginning upload of '$IMG' to DRP Files as 'images/${build_name}.img.xz' - be patient"
drpcli files upload "$IMG" as "images/${build_name}.img.xz" || true

exit 0
