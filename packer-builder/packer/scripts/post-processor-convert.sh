#!/usr/bin/env bash
# post-process a packer qcow2 image to raw and compress it

set -e
xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

build_dir=$1
complete_dir=$2
mkdir -p $complete_dir

# manifest comes from the packer post processor.
[[ -f "manifest.json" ]] || xiterr 1 "manifest.json not found."
# get the name of the image file from the manifest
build_path=$( cat manifest.json | jq -r '.builds[0].files[0].name')
build_name=$(basename $build_path)

[[ -f "$build_path" ]] || xiterr 1 "Build artifact '$build_path' not found."
[[ -d $build_dir ]] || xiterr 1 "Build directory '$build_dir' not found."
[[ ! -f "$complete_dir/$build_name.img.xz" ]] || xiterr 1 "Build artifact '$complete_dir/$build_name.img.xz' should not exist yet."

# move to complete and exit if type is ova
if [[ $build_name == *.ova ]]; then
    echo "Skipping conversion on OVA image: $build_name"
    mv $build_path $complete_dir
    exit 0
fi

which qemu-img > /dev/null 2>&1 || xiterr "no 'qemu-img' found, install and try again."

# convert image from any format to raw, may fail to detect source image
# format but will have a good error message
qemu-img convert -O raw $build_dir/$build_name $complete_dir/$build_name.img

# compress the image, using all available cores
# TODO: parameterize cpus and compression level
time xz -T 0 -8 -z $complete_dir/$build_name.img

echo "Build artifact ready:  $complete_dir/$build_name.img.xz"
ls -lh "$complete_dir/$build_name.img.xz"
exit 0