# packer

This builds a set of OSes plus the RackN Digital Rebar Agent using
[Packer](https://www.packer.io/).  The intent is provide a set of
images for use with the Digital Rebar Platform that customers may use,
clone, and build upon.

This repository is consumed by the `packer-builder` content bundle to
provide a complete solution for building images.

## Git Get the Packer Builder Content

Requires `git` (to get this repo!).

This repo is part of the Digital Rebar Provision Content found at
[provision-content](https://gitlab.com/rackn/provision-content).
Since that repository is quite large, you may want to pull only the 
packer build content with the following:

```shell
git init
git remote add origin https://gitlab.com/rackn/provision-content.git
git fetch origin
git checkout origin/v4 -- packer-builder/packer
```

This will produce a populated directory with the Packer build pieces in
`packer-build/packer/` location.

## Setup

The preferred method to build images is to use the `packer-builder` content
from within the Digital Rebar Platform. See the [packer-builder README](../README.md).

***Note***: the following local setup scripts are untested and may not work as expected.

To run packer builds independently, you will need to setup the build environment.
Run the following scripts to get Packer and setup KVM/Qemu:

* `setup-tools.sh` # sets up packer, etc.
* `setup-kvm.sh`  # sets up KVM, Qemu, and base tools
* `setup-virtualbox.sh` # sets up VirtualBox and base tools

`make` is used to initiate the image builds.  Use `make help` for build targets.

## Example

  1. On an Ubuntu 22.04 system
  2. Clone this repository
  3. Install basic tools, similar to the `setup-tools.sh` script
  4. Choose to install a hypervisor
     1. Install the hypervisor similar to the `setup-kvm.sh` or `setup-virtualbox.sh` scripts
  5. use make to build an image

## An Important Service Announcement on Disk Space

Builds can take a huge amount of space if you have multiple builds happening.
Current builds may create a copy of the image for total disk space usage of 2x the image size.
Some images are 20GB in size, so you may need 40GB of free space to build them.
If there are more than one build happening, this grows quickly.
Plan on the build machine having at least 100GB of free space.
