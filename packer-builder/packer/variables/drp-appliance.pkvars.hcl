vm_name = "drp-appliance"
disk_size = "15000"
disk_additional_size = []
switch_name = "Default Switch"
output_vagrant = "./vbox/drp-trial.box"
vlan_id = ""
memory = "4096"
cpus = "4"
vagrantfile_template = "./vagrant/hv_drp_trial.template"
ssh_password = "rackndeploy"
ssh_username = "root"
provision_script_options = "-z false"

uefi_file="extra/files/almalinux/9/uefi.sh"
headless = false