---
Name: "redhat/kickstart-syspurpose"
Description: "RedHat 9 Kickstart directive 'syspurpose' configuration."
Documentation: |
  Specifies the Redhat Subscription Manager based `syspurpose` configuration
  directive for Kickstart installations.  This is used only in Redhat 9 and
  newer based Kickstarts.

  The param is a set of key and value pairs that are converted to the Kickstart
  `syspurpose` command format.  The basic structure is:

    * `key`: `value`

  Each `key` is turned in to double dash argument flag (eg `--key=value`) set
  of arguments appended to the `syspurpose` kickstart directive.  For example:

    * `syspurpose --key=value`

  As of January 2024, the only supported arguments are:

    * `role`
    * `sla`
    * `usage`

  To achieve a `syspurose` definition as follows:

    * `syspurpose --role="Red Hat Enterprise Linux Server" --sla="Premium" --usage="Production"`

  Example in JSON format:

    ```json
    "redhat/kickstart-syspurpose": {
        "role": "Red Hat Enterprise Linux Server",
        "sla": "Premium",
        "usage": "Production"
      }
    ```

  Example in YAML format:

    ```yaml
    redhat/kickstart-syspurpose:
      role: "Red Hat Enterprise Linux Server"
      sla: "Premium"
      usage: "Production"
    ```

  By default no `syspurpose` directive will be set in the kickstart unless this Parameter is
  defined during installtion time.

  This Parameter is *only* used in RHEL Server 9 kickstart templates.  Additional information
  and documentation can be found on Redhat's website at:

    * https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/performing_an_advanced_rhel_9_installation/index#configuring-system-purpose-advanced_installing-rhel-as-an-experienced-user

Meta:
  icon: "file"
  color: "blue"
  title: "Digital Rebar Community Content"
Schema:
  type: object
  default: {}
  additionalProperties:
    type: string
