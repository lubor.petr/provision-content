# DigitalRebar Provision RHEL-7 (and related distros) kickstart

{{range .InstallRepos}}
{{ .Install }}
{{end}}
# key --skip
# Disable geolocation for language and timezone
# Currently broken by https://bugzilla.redhat.com/show_bug.cgi?id=1111717
# geoloc 0
timezone --utc UTC
lang en_US.UTF-8
keyboard us
# rebar
rootpw --iscrypted {{if .ParamExists "provisioner-default-password-hash"}}{{.Param "provisioner-default-password-hash"}}{{else}}$6$drprocksdrprocks$upAIK9ynEEdFmaxJ5j0QRvwmIu2ruJa1A1XB7GZjrnYYXXyNr4qF9FttxMda2j.cmh.TSiLgn4B/7z0iSHkDC1{{end}}
firewall --disabled
authconfig --enableshadow --enablemd5
selinux --{{.Param "provisioner-selinux"}}

{{if .ParamExists "part-scheme" -}}
{{$templateName := (printf "part-scheme-%s.tmpl" (.Param "part-scheme")) -}}
{{.CallTemplate $templateName .}}
{{else -}}
{{template "part-scheme-default.tmpl" .}}
{{end -}}

text
reboot {{if .Param "kexec-ok" }}--kexec{{end}}

%packages
@core
trousers
fipscheck
device-mapper-multipath
openssh
curl
efibootmgr
tar
{{if .ParamExists "extra-packages" -}}
{{ range $index, $element := (.Param "extra-packages") -}}
{{$element}}
{{end -}}
{{end -}}
%end

%post

exec > /root/post-install.log 2>&1
set -x
export PS4='${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '

{{template "reset-workflow.tmpl" .}}
{{template "runner.tmpl" .}}

sync
%end
