#!/usr/bin/env bash
# Sets up RedHat Subscription Manager and activates repos

{{ template "setup.tmpl" .}}

{{ if eq (.Param "redhat/subscription-skip-activation") true }}echo "Skipping RHSM setup."; exit 0{{ end }}
{{ if and ( eq "trial" ( .Param "redhat/subscription-username" ) ) ( not ( .ParamExists "redhat/subscription-password" ) ) -}}echo "Skipping RHSM setup due to trial username and no password parameter."; exit 0{{ end }}

(which subscription-manager > /dev/null 2>&1 ) || xiterr "Unable to locate 'subscription-manager' tool in PATH ($PATH)."

{{ if .ParamExists "redhat/subscription-username" }}UN={{ .Param "redhat/subscription-username" }}{{ else }}ERR=1{{ end }}
{{ if .ParamExists "redhat/subscription-password" }}PW={{ .Param "redhat/subscription-password" }}{{ else }}ERR=1{{ end }}
(( $ERR )) && xiterr "Missing RHSM username or password info ('redhat/subscription-username' and 'redhat/subscription-password')"

# Attempt to check the list based Param first for an OS-Ver index entry
# to use for Repository activation; otherwise, fallback to the default
# redhat/subscription-repos based single param
# set_repos will use TAG variable (eg "rhel-9") as the source list for
# activating package repositories

set_repos() {
{{ if .ParamExists "redhat/subscription-repos-list" -}}
# using the newer redhat/subscription-repos-list Param

{{ range $tag, $repos := .Param "redhat/subscription-repos-list" -}}
if [[ "{{ $tag }}" == "$TAG" ]]
then
  TAG_SEEN="true"
  {{ range $repo := $repos }}
  REPOS="$REPOS --enable={{ $repo }}"
  {{ end -}}
fi
{{ end -}}

if [[ "$TAG_SEEN" != "true" ]]
then
  xiterr 1 "Unable to find index tag '$TAG' in Param 'redhat/subscription-repos-list'."
fi

{{ else -}}

echo "Falling back to using the old Param 'redhat/subscription-repos' as the"
echo "newer 'redhat/subscription-repos-list' does not exist on the system"

{{ range $key, $repos := .Param "redhat/subscription-repos" -}}
echo "Adding '{{ $repos }}' to the activated subscription manager repositories."
REPOS="$REPOS --enable={{ $repos }}"
{{ end -}}
{{ end -}}

} # end set_repos()

{{ if .Param "redhat/subscription-tag-override" -}}
TAG='{{ .ParamExpand "redhat/subscription-tag-override" }}'
echo "Subscription repo tag has been overridden to '$TAG'"
{{ else -}}
source /etc/os-release
if [[ "$ID" == "rhel" ]]
then
  V=$(echo "$VERSION_ID" | cut -d '.' -f1)
  TAG="${ID}-${V}"
  set_repos
else
  echo "NOTICE: '/etc/os-release' did not report ID = 'rhel'."
  echo "        Skipping repos subscription enrollment.  Evaluate the Params"
  echo "        redhat/subscription-repost-list or redhat/subscription-repos"
fi
{{ end -}}

[[ -n "$REPOS" ]] && DO_REPOS="subscription-manager repos $REPOS" || DO_REPOS=""

subscription-manager register --force --username="$UN" --password="$PW"
subscription-manager attach --auto
subscription-manager list

ORIGINAL_POLICY=$(update-crypto-policies --show)
{{ if .Param "redhat/subscription-crypto-policy-override" -}}
update-crypto-policies --set {{ .Param "redhat/subscription-crypto-policy-override" }}
{{ end -}}
{{ range $key, $gpgkey := .Param "redhat/subscription-gpg-keys" -}}
rpm --import {{ $gpgkey }}
{{ else -}}
echo "No GPG keys for import found. (Param: 'redhat/subscription-gpg-keys')."
{{ end -}}
{{ if .Param "redhat/subscription-crypto-policy-override" -}}
update-crypto-policies --set $ORIGINAL_POLICY
{{ end -}}

[[ -n "$DO_REPOS" ]] && eval $DO_REPOS || echo "No repositories found to be added."
yum -y makecache

exit 0
