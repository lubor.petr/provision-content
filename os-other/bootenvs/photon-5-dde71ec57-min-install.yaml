---
Name: photon-5-dde71ec57-min-install
Description: VMware Photon OS 5.0 minimal ISO deployment boot environment.
Documentation: |
  This BootEnv installs the VMware Photon 5.0 distribution from the "minimal"
  ISO.

    * ISOS can be found at: <https://packages.vmware.com/photon/>
    * Documentation at: <https://vmware.github.io/photon/docs-v5/administration-guide/photon-rpm-ostree/installing-a-host-against-custom-server-repository/#automated-install-of-a-custom-host-via-kickstart>
    * Kickstart Doc: <https://vmware.github.io/photon/docs-v5/user-guide/working-with-kickstart/>

Meta:
  color: blue
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: Photon
OS:
  Family: "redhat"
  Codename: "photon"
  Name: "photon-5-dde71ec57-min"
  Version: "5"
  SupportedArchitectures:
    x86_64:
      BootParams: >-
        root=/dev/ram0 ks={{.SecureProvisionerURL}}/{{.Machine.Path}}/compute.ks loglevel=3 insecure_installation=1
        repo={{.Env.InstallUrl}}/RPMS
        {{.Param "kernel-options"}} -- {{.Param "kernel-console"}}
      Initrds:
        - isolinux/initrd.img
      IsoFile: photon-minimal-5.0-dde71ec57.x86_64.iso
      IsoUrl: https://packages.vmware.com/photon/5.0/GA/iso/photon-minimal-5.0-dde71ec57.x86_64.iso
      Kernel: isolinux/vmlinuz
      Sha256: "691d09eb61f8cad470f21c88287ff6b005c3be365c926a87577e714aee2d46bc"
    aarch64:
      BootParams: >-
        root=/dev/ram0 ks={{.SecureProvisionerURL}}/{{.Machine.Path}}/compute.ks loglevel=3 insecure_installation=1
        repo={{.Env.InstallUrl}}/RPMS
        {{.Param "kernel-options"}} -- {{.Param "kernel-console"}}
      Initrds:
        - isolinux/initrd.img
      IsoFile: photon-minimal-5.0-dde71ec57.aarch64.iso
      IsoUrl: https://packages.vmware.com/photon/5.0/GA/iso/photon-minimal-5.0-dde71ec57.aarch64.iso
      Kernel: isolinux/vmlinuz
      Sha256: "b5b3c644a9a3d59d259db45595e85be0f4be80356f2f32feb7270343ad46c20e"
OnlyUnknown: false
OptionalParams:
  - operating-system-disk
  - provisioner-default-password-hash
  - kernel-console
  - kernel-options
  - proxy-servers
  - select-kickseed
  - kickstart-base-packages
  - extra-packages
RequiredParams: []
Templates:
  - ID: kexec.tmpl
    Name: kexec
    Path: "{{.Machine.Path}}/kexec"
  - ID: default-pxelinux.tmpl
    Name: pxelinux
    Path: pxelinux.cfg/{{.Machine.HexAddress}}
  - ID: default-ipxe.tmpl
    Name: ipxe
    Path: "{{.Machine.Address}}.ipxe"
  - ID: default-pxelinux.tmpl
    Name: pxelinux-mac
    Path: pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}
  - ID: default-ipxe.tmpl
    Name: ipxe-mac
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - ID: default-grub.tmpl
    Name: grub
    Path: grub/{{.Machine.Address}}.cfg
  - ID: default-grub.tmpl
    Name: grub-mac
    Path: grub/{{.Machine.MacAddr "grub"}}.cfg
  - ID: default-grub.tmpl
    Name: grub-secure
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
  - ID: "select-kickseed.tmpl"
    Name: "compute.ks"
    Path: "{{.Machine.Path}}/compute.ks"
  - Name: reset-workflow-runner.sh
    Path: "{{.Machine.Path}}/reset-workflow-runner.sh"
    Contents: |
      {{template "reset-workflow.tmpl" .}}
      {{template "runner.tmpl" .}}
