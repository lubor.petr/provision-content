---
Name: rhel-server-8.7-dvd-install
Description: Install BootEnv for RedHat 8.7 - full DVD ISO
Documentation: |
  This BootEnv installs the RHEL Server 8.7 operating system from the full dvd
  ISO. By default, it will install as a trial with no registration. This is
  specified by the `redhat/subscription-username` parameter with the default
  username of "trial" and `redhat/subscription-password` blank. Adding a
  password and/or changing the username will be verified by RedHat's servers
  and the install will hang if it fails. You can also completely skip
  registration even with the username and password parameters set by adding
  the `redhat/subscription-skip-activation` parameter.

  The ISO can be downloaded from the RedHat Access website with an authorized
  login and account.  The website is typically found at:

    * <https://access.redhat.com/downloads/content/479/ver=/rhel---8/>

Meta:
  color: red
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: RedHat
OS:
  Name: rhel-server-8.7-dvd
  Family: redhat
  Codename: ""
  Version: "8"
  SupportedArchitectures:
    x86_64:
      IsoFile: "rhel-8.7-x86_64-dvd.iso"
      Sha256: "a6a7418a75d721cc696d3cbdd648b5248808e7fef0f8742f518e43b46fa08139"
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
OnlyUnknown: false
RequiredParams: []
OptionalParams:
  - kernel-console
  - kernel-options
  - ntp-servers
  - operating-system-disk
  - proxy-servers
  - provisioner-default-password-hash
  - provisioner-default-username
  - provisioner-access-key
  - provisioner-network-config
  - kickstart-base-packages
  - extra-packages
  - redhat/kickstart-shell
  - redhat/rhsm-activation-key
  - redhat/rhsm-additional
  - redhat/rhsm-organization
  - redhat/subscription-gpg-keys
  - redhat/subscription-password
  - redhat/subscription-repos
  - redhat/subscription-skip-activation
  - redhat/subscription-username
Templates:
  - Name: kexec
    ID: kexec.tmpl
    Path: "{{.Machine.Path}}/kexec"
  - Name: pxelinux
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.HexAddress}}
  - Name: ipxe
    ID: default-ipxe.tmpl
    Path: "{{.Machine.Address}}.ipxe"
  - Name: pxelinux-mac
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}
  - Name: ipxe-mac
    ID: default-ipxe.tmpl
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - Name: grub
    ID: default-grub.tmpl
    Path: grub/{{.Machine.Address}}.cfg
  - Name: grub-mac
    ID: default-grub.tmpl
    Path: grub/{{.Machine.MacAddr "grub"}}.cfg
  - ID: default-grub.tmpl
    Name: grub-secure
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
  - Name: compute.ks
    ID: select-kickseed.tmpl
    Path: "{{.Machine.Path}}/compute.ks"
