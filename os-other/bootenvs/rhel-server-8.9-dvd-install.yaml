---
Name: rhel-server-8.9-dvd-install
Description: Install BootEnv for RedHat 8.9 - full DVD ISO
Documentation: |
  This BootEnv installs the RHEL Server 8.9 operating system from the full dvd
  ISO. By default, it will install as a trial with no registration. This is
  specified by the `redhat/subscription-username` parameter with the default
  username of "trial" and `redhat/subscription-password` blank. Adding a
  password and/or changing the username will be verified by RedHat's servers
  and the install will hang if it fails. You can also completely skip
  registration even with the username and password parameters set by adding
  the `redhat/subscription-skip-activation` parameter.

  The ISO can be downloaded from the RedHat Access website with an authorized
  login and account.  The website is typically found at:

    * <https://access.redhat.com/downloads/content/479/ver=/rhel---8/8.9/x86_64/product-software>
    * <https://access.redhat.com/downloads/content/419/ver=/rhel---8/8.9/aarch64/product-software>

Meta:
  color: red
  feature-flags: change-stage-v2
  group-by: RedHat
  icon: linux
  title: Digital Rebar Community Content
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: rhel-server-8.9-dvd
  Family: redhat
  Codename: ""
  Version: "8.9"
  SupportedArchitectures:
    x86_64:
      IsoFile: "rhel-8.9-x86_64-dvd.iso"
      Sha256: "c4fd0632ce15a7d56e1d174176456943bd48306f9d35bcecbcb0a1dc49088e23"
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
    aarch64:
      IsoFile: "rhel-8.9-aarch64-dvd.iso"
      Sha256: "db49b3d58cad98aa43063a76d3bda0076571b86d06f03bb723d0a82623ba2276"
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.Param "kernel-options"}}
        --
        {{.Param "kernel-console"}}
OptionalParams:
  - kernel-console
  - kernel-options
  - ntp-servers
  - operating-system-disk
  - proxy-servers
  - provisioner-default-password-hash
  - provisioner-default-username
  - provisioner-access-key
  - provisioner-network-config
  - kickstart-base-packages
  - extra-packages
  - redhat/kickstart-shell
  - redhat/rhsm-activation-key
  - redhat/rhsm-additional
  - redhat/rhsm-organization
  - redhat/subscription-gpg-keys
  - redhat/subscription-password
  - redhat/subscription-repos
  - redhat/subscription-skip-activation
  - redhat/subscription-username
Templates:
  - Name: kexec
    ID: kexec.tmpl
    Path: "{{.Machine.Path}}/kexec"
  - Name: pxelinux
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.HexAddress}}
  - Name: ipxe
    ID: default-ipxe.tmpl
    Path: "{{.Machine.Address}}.ipxe"
  - Name: pxelinux-mac
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}
  - Name: ipxe-mac
    ID: default-ipxe.tmpl
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - Name: grub
    ID: default-grub.tmpl
    Path: grub/{{.Machine.Address}}.cfg
  - Name: grub-mac
    ID: default-grub.tmpl
    Path: grub/{{.Machine.MacAddr "grub"}}.cfg
  - ID: default-grub.tmpl
    Name: grub-secure
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
  - Name: compute.ks
    ID: select-kickseed.tmpl
    Path: "{{.Machine.Path}}/compute.ks"
