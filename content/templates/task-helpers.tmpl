

# This should NOT be used.  Focus on error.
function job_fatal() {
  echo "FATAL: $@"
}

# This function appends to the jobs ResultErrors field
function job_error() {
  echo "ERROR: $@"
  drpcli jobs show {{.CurrentJob}} | jq --arg a "$@" '.ResultErrors += [$a]' > job.json
  drpcli jobs update {{.CurrentJob}} job.json >/dev/null
  rm -f job.json
}

function job_info() {
  echo "INFO: $@"
}

function job_warn() {
  echo "WARN: $@"
}

function job_debug() {
  {{ if or (.Param "rs-debug-enable") (.Param "rs-debug-output") }}
  echo "DEBUG: $@"
  {{ else }}
  # Do nothing
  true
  {{ end }}
}

# Prints a success message and exits 0
function job_success() {
  echo "Success: $@"
  exit 0
}

# TODO: This needs to be fixed.
#
# Using the task-store-result-in-parameter and task-merge-result-into-params parameters
# to update the machine/cluster/resource-broker or work_order with the results.
#
function task_record() {
  echo "Not implemented: $@"
}

# Uses drpcli without a token or a proxy
# The RS_TOKEN and RS_LOCAL_PROXY will be unset
function drpcli_raw() {
  RS_TOKEN= drpcli --ignore-unix-proxy "$@"
}

# Function to test if a command exists.
# Usage:
# if ! command_exists dig; then
#   job_error "dig is not installed. Please install it and try again."
#   exit 1
# fi
function command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Periodic job functions: Print messages at specified intervals
#
# Usage:
#   job_info_periodic <message> [time_limit_seconds]
#   job_warn_periodic <message> [time_limit_seconds]
#   job_debug_periodic <message> [time_limit_seconds]
#   
#   For dynamic messages:
#   job_info_periodic <message_template> [time_limit_seconds] [arg1] [arg2] ...
#   job_warn_periodic <message_template> [time_limit_seconds] [arg1] [arg2] ...
#   job_debug_periodic <message_template> [time_limit_seconds] [arg1] [arg2] ...
#
# Parameters:
#   message: The static message to print
#   message_template: The message template to print (can include printf-style format specifiers)
#   time_limit_seconds: Minimum time between prints (default: 60 seconds)
#   arg1, arg2, ...: Optional arguments to fill in the message template
#
# Examples:
#   # Static message, prints every 60 seconds (default)
#   job_info_periodic "System check: All services running"
#
#   # Static message with custom timing, prints every 30 seconds
#   job_warn_periodic "Warning: System under high load" 30
#
#   # Dynamic message with a counter, prints every 15 seconds
#   for i in {1..100}; do
#     job_debug_periodic "Debug: Loop iteration %d" 15 $i
#     sleep 1
#   done
#
#   # Multiple variables, prints every 20 seconds
#   while true; do
#     cpu=$(get_cpu_usage)
#     mem=$(get_memory_usage)
#     job_info_periodic "Status: CPU: %.1f%%, Memory: %.1f%%" 20 $cpu $mem
#     sleep 1
#   done
#   
#   # Message with variable parts
#   for i in {1..60}; do
#     job_info_periodic "Current count: %d" 10 $i
#     job_warn_periodic "Warning %s: Value is %d" 30 "HIGH" $((i*10))
#     job_debug_periodic "Debug: %s = %d, %s = %f" 15 "Count" $i "Average" $(echo "$i / 2" | bc -l)
#     sleep 1
#   done
#
# Note: For dynamic messages, the timing is based on the message template, 
# not the formatted message. Messages with the same template but different 
# variable values are treated as the same message for timing purposes.

function job_info_periodic() {
  job_message_periodic job_info "${1}" "${2:-60}" "${@:3}"
}

function job_warn_periodic() {
  job_message_periodic job_warn "${1}" "${2:-60}" "${@:3}"
}

function job_debug_periodic() {
  job_message_periodic job_debug "${1}" "${2:-60}" "${@:3}"
}

declare -A job_message_last_print_times

function job_message_periodic() {
  local func="${1}"
  local message_template="${2}"
  local time_limit="${3}"
  local current_time=$(date +%s)
  local key="${func}:${message_template}"

  if [[ -z ${job_message_last_print_times[${key}]} ]] || (( current_time - ${job_message_last_print_times[${key}]} >= time_limit )); then
    # Format the message with any additional arguments
    local formatted_message=$(printf "${message_template}" "${@:4}")
    ${func} "${formatted_message}"
    job_message_last_print_times[${key}]=${current_time}
  fi
}
