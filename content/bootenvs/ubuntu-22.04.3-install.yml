---
Name: ubuntu-22.04.3-install
Description: Ubuntu-22.04.3 install
Documentation: |
  Install Ubuntu Jammy Jellyfish (22.04.3) LTS version.  This BootEnv will install
  the General Available (GA) kernel.

  !!! note
      Default Ubuntu ISOs will attempt to check internet repositories, this
      can cause problems during provisioning if your environment does not have
      outbound access.  Workaround this by defining Options 3 (Gateway) and 6 (DNS)
      for your Subnet settings.  See <https://docs.rackn.io/stable/resources/kb/kb-00033/>

  The Param named `part-scheme` can be used to inject a storage section.
  Set the param to a value that will be used to locate a template containing
  the YAML `storage` definition.

  The template would be named "part-scheme-`<Value of part-scheme>`.tmpl".

  The format should be:

    ```yaml
    # Note Indentation matters with the extra two spaces.
      storage:
        swap:
          size: 0
        layout:
          name: direct
          match:
            ssd: yes
    ```

  Documentation for `storage` format and options can be found at:

    * <https://docs.rackn.io/stable/redirect/?ref=rs_image_deploy_partitioning>
    * <https://curtin.readthedocs.io/en/latest/topics/storage.html>

  The Ubuntu 20.04 enforced UEFI boot re-ordering can be corrected by
  **adding** this additional stanza to the `storage` YAML structure:

    ```yaml
    # Note Indentation matters with the extra two spaces.
      storage:
        grub:
          reorder_uefi: False
    ```

  !!! note
      If any `storage` definition is used, then the all options for
      setting disk partitioning **must** be used.  The above UEFI disable
      can not be used on it's own without additional disk configuration.
Meta:
  color: purple
  feature-flags: change-stage-v2
  group-by: Ubuntu
  icon: linux
  title: Digital Rebar Community Content
OS:
  Name: ubuntu-22.04.3
  Codename: Jammy Jellyfish
  Family: ubuntu
  Version: "22.04.3"
  SupportedArchitectures:
    amd64:
      IsoFile: ubuntu-22.04.3-live-server-amd64.iso
      IsoUrl: https://old-releases.ubuntu.com/releases/22.04/ubuntu-22.04.3-live-server-amd64.iso
      Sha256: a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd
      Kernel: casper/vmlinuz
      Initrds:
        - casper/initrd
      BootParams: >-
        root=/dev/ram0 ramdisk_size=1500000
        ip=dhcp
        {{ if .Param "ipv6-support" }}ipv6.disable=0{{ else }}ipv6.disable=1{{ end }}
        url={{ .ProvisionerURL }}/isos/{{ .Env.OS.SupportedArchitectures.amd64.IsoFile }}
        cloud-config-url={{ .ProvisionerURL }}/machines/{{ .Machine.UUID }}/autoinstall/meta-data
        {{.ParamExpand "kernel-options"}}
OptionalParams:
  - part-scheme
  - operating-system-disk
  - provisioner-default-user
  - provisioner-default-fullname
  - provisioner-default-uid
  - provisioner-default-password-hash
  - kernel-options
  - proxy-servers
  - dns-domain
  - local-repo
  - proxy-servers
  - ntp-servers
  - select-kickseed
  - select-metadata
Templates:
  - ID: "kexec.tmpl"
    Name: "kexec"
    Path: "{{.Machine.Path}}/kexec"
  - ID: "canonical-autoinstall-pxelinux.tmpl"
    Name: "pxelinux"
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
  - ID: "canonical-autoinstall-ipxe.tmpl"
    Name: "ipxe"
    Path: "{{.Machine.Address}}.ipxe"
  - ID: "canonical-autoinstall-pxelinux.tmpl"
    Name: "pxelinux-mac"
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
  - ID: "canonical-autoinstall-ipxe.tmpl"
    Name: "ipxe-mac"
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: canonical-autoinstall-grub.tmpl
  - ID: "canonical-autoinstall-grub.tmpl"
    Name: "grub"
    Path: "grub/{{.Machine.Address}}.cfg"
  - ID: "canonical-autoinstall-grub.tmpl"
    Name: "grub-mac"
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
  - ID: "select-kickseed.tmpl"
    Name: "autoinstall"
    Path: "{{.Machine.Path}}/autoinstall/user-data"
  - ID: "select-metadata.tmpl"
    Name: "meta-data"
    Path: "{{.Machine.Path}}/autoinstall/meta-data"
  - Contents: |
      #!/bin/bash
      set -x
      export PS4='${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '

      {{template "reset-workflow.tmpl" .}}
      {{template "runner.tmpl" .}}
    Name: post-install
    Path: "{{.Machine.Path}}/post-install.sh"
