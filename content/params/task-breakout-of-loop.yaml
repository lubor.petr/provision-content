---
Name: task-breakout-of-loop
Description: Attempts to break from controling Tasks loop construct if set to true.
Documentation: |
  This Parameter will attempt to break out of the controlling
  Tasks loop construct if set to `true`.

  It is important to note that a Task script must be configured to
  use this Param flag as part of it's code.  It is generally used
  inside loop constructs to test if the controlling script should
  be exited with an error by operator intervention.

  To use this construct, the Task must have the Template named
  `task-breakout-of-loop.sh.tmpl` included inside the controlling
  loop.  An example implementation:

    ```sh
    # to exit this loop, set 'Runnable: false' or set
    # Machine Param 'task-breakout-of-loop' to 'true'.
    while :
    do
        {{ template "task-breakout-of-loop.sh.tmpl" . }}
        echo "infinite loop"
        sleep 5
    done
    ```

  If the Param on the Machine is set as follows:

    * `task-breakout-of-loop: true`

  Then on the next iteration of the loop, the script will
  exit with error code `1`.

  Additionally, the `template` will evaluate the Machine's
  `Runnable` field.  If `false`, the script will break out
  with an error code, regardless of the Param value setting.

Schema:
  type: boolean
  default: false
