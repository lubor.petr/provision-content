---
Name: record-current-uefi-boot-entry
Description: "Record the current UEFI boot entry"
Documentation: |
  While we are in Sledgehammer or in a network-based OS install
  environment, record the current UEFI boot entry.  We will use this later in the
  machine lifecycle to set the UEFI boot order back to PXE first from the
  entry this task will record.
Meta:
  type: "setup"
  title: Digital Rebar Community Content
  icon: "disk outline"
  color: "blue"
Templates:
  - Name: record-current-uefi-boot-entry
    Contents: |
      #!/usr/bin/env bash
      umount_things() {
          if [[ $efiVarsMounted ]]; then
              umount /sys/firmware/efi/efivars || :
          fi
          if [[ $sysMounted ]]; then
              umount /sys || :
          fi
      }
      trap umount_things EXIT
      if [[ ! -d /sys/firmware ]]; then
          mount -t sysfs sysfs /sys
          sysMounted=true
      fi
      if [[ ! -d /sys/firmware/efi ]]; then
          echo "No EFI firmware, nothing to do"
          exit 0
      fi
      if [[ $(echo /sys/firmware/efi/efivars/*) = '/sys/firmware/efi/efivars/*' ]]; then
          mkdir -p /sys/firmware/efi/efivars
          mount -t efivarfs efivarfs /sys/firmware/efi/efivars
          efiVarsMounted=true
      fi
      if ! which efibootmgr; then
          echo "Missing efibootmgr, please install it as part of your OS install"
          exit 1
      fi
      efibootmgr -v || :
      entry="$(efibootmgr |awk '/^BootCurrent/ {print $2}')"
      if [[ ! $entry ]]; then
          echo "Missing BootCurrent boot entry, falling back to trying to determine the proper entry heuristically"
          echo "If your UEFI firmware is up-to-date, you need to file a bug report with your system vendor to"
          echo "have them expose the BootCurrent UEFI variable as defined in the UEFI spec:"
          echo "   https://uefi.org/specs/UEFI/2.10/03_Boot_Manager.html#globally-defined-variables   "
          entry="$(efibootmgr |awk '/^Boot[0-9A-F]{4}\* /' |egrep -i '{{.Param "current-boot-entry-heuristic-regex"}}' |head -1 |cut -c 5-8)"
      fi
      if [[ ! $entry ]]; then
          echo "Cannot determine which UEFI boot entry to move to the top of the list. Leaving current settings intact."
          exit 0
      fi
      fullPath="$(efibootmgr |awk "/^Boot$entry/ {print}")"
      drpcli machines set "$RS_UUID" param current-boot-entry to "\"$entry\""
      drpcli machines set "$RS_UUID" param current-boot-entry-fullpath to "\"$fullPath\""