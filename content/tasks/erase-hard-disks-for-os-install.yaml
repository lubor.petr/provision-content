---
Name: "erase-hard-disks-for-os-install"
Description: |
  Erases any data on the hard disks that might confuse the OS install
  process.  This includes LVM metadata, partition tables, software RAID signatures,
  and the first and last megabyte of any partitions and disks.
Meta:
  type: "erase"
  feature-flags: "sane-exit-codes"
  icon: "erase"
  color: "blue"
  title: "Digital Rebar Community Content"
RequiredParams:
  - zero-hard-disks-for-os-install
Templates:
  - Name: "erase-disks"
    Contents: |
      #!/bin/bash
      # Zero hard disks for OS install

      ### pull in helper script and job log task-helpers
      {{ template "setup.tmpl" . }}
      ### start erase-disks

      # Nuke it all.
      declare lv vg pv maj min blocks name bdev

      umount -a || :

      # deactivate and then forcibly remove any logical volumes
      lvscan --ignorelockingfailure -P
      while read lv
      do
          echo "Removing LVM partition '$lv'"
          umount --force "$lv" 2> /dev/null || :
          wipefs -a -f "$lv" || :
          lvchange -a n "$lv" || :
          lvremove --force "$lv" || :
      done < <(lvs --noheadings -o lv_path)

      # Deactivate all known lvm and dm devices first, unmounting filesystems as needed.
      job_info "Deactivating all known LVM partitions"
      blkdeactivate -u -d force,retry -l retry,wholevg || :

      # Make sure that the kernel knows about all the partitions
      for bd in /sys/block/*
      do
          [[ -b /dev/${bd##*/} ]] || continue
          grep -q 'devices/virtual' < <(readlink "$bd") && continue
          echo "Probing for all partitions on dev/${bd##*/}.  Failures are OK."
          partprobe "/dev/${bd##*/}" || :
      done

      # Zap any volume groups that may be lying around.
      vgscan --ignorelockingfailure -P
      while read vg
      do
          echo "Forcibly removing volume group '$vg'"
          vgremove -ff -y "$vg" || :
      done < <(vgs --noheadings -o vg_name)

      # Wipe out any LVM metadata that the kernel may have detected.
      pvscan --ignorelockingfailure
      while read pv
      do
          echo "Forcibly removing physical volume '$pv'"
          pvremove -ff -y "$pv" || :
      done < <(pvs --noheadings -o pv_name)

      # Now zap any partitions along with any RAID metadata that may exist.
      job_info "Zap any partitions and RAID metadata"

      while read maj min blocks name
      do
          [[ -b /dev/$name && -w /dev/$name && $name != name ]] || continue
          [[ $name = loop* ]] && continue
          [[ $name = fd* ]] && continue
          echo "Forcibly removing any RAID metadata on /dev/$name. Failures are OK if readonly"
          mdadm --misc --zero-superblock --force /dev/$name || :
          wipefs -a -f /dev/$name || :

          if (( blocks >= 4096))
          then
              echo "Zeroing the first and last 2 megs of /dev/$name. Failures are OK if readonly"
              dd "if=/dev/zero" "of=/dev/$name" "bs=512" "count=4096" || :
              dd "if=/dev/zero" "of=/dev/$name" "bs=512" "count=4096" "seek=$(($blocks - 4096))" || :
          else
              echo "Zeroing small device /dev/$name.  Failures are OK if readonly"
              dd "if=/dev/zero" "of=/dev/$name" "bs=512" "count=$blocks" || :
          fi
      done < <(tac /proc/partitions)

      ###
      #  Attempt to forcibly unmount filesystems if mounted, perform a blckdiscard
      #  on the device, and then if possible secure discard, failing a secure discard
      #  perform a normal discard of blocks.
      #  
      #  Requires the device to operate on as ARGv1 (eg "discard /dev/sda')
      ###
      discard() {
          local _bd="$1"
          local _ret=true
          local _mounted _dev _spinner _want_zero _skip_zero

          [[ ! -b /dev/${_bd##*/} ]] && _continue="false" || true
          grep -q 'devices/virtual' < <(readlink "$_bd") && _continue="false" || true
          _dev="/dev/${_bd##*/}"

          if [[ "$_continue" == "false" ]]
          then
            job_info "[$_dev] Not discarding (not a block dev or is a virtual dev)"
            return
          else
            job_info "[$_dev] Start discard process in the background"
          fi

          for _mounted in $(lsblk --list -no NAME,MOUNTPOINT $_dev | grep "/" | awk ' { print $NF } ')
          do
              echo "[$_dev] Attempting to forcibly unmount '$_mounted'"
              umount -f $_mounted || :
              partprobe
          done

          _spinner=$(cat "$_bd/queue/rotational")
          _want_zero="{{.Param "zero-hard-disks-for-os-install"}}"
          _skip_zero=$(cat "$_bd/queue/discard_zeroes_data")

          # blkdiscard -z does the same job as dd if=/dev/zero, except the
          # kernel does the work and it uses the SCSI command WRITE_SAME if
          # device supports it, which can greatly speed up the zeroing process
          if [[ $_want_zero = true && $_skip_zero != 1 && $_spinner = 1 ]]
          then
              echo "[$_dev] Zeroing device with 'blkdiscard -z'"
              blkdiscard -z "$_dev" || :
          fi

          # if discard_max_bytes is zero, then blkdiscard will not work anyways.
          # Try secure erase first, then regular discard.
          if [[ $(cat "$_bd/queue/discard_max_bytes") != 0 ]]
          then
              echo "[$_dev] Attempting secure discard"
              echo "[$_dev] This may fail if device does not support secure discard"

              if blkdiscard -s "$_dev"
              then
                  echo "[$_dev] Secure discard finished"
              else
                  echo "[$_dev] Sercure discard failed, attempting normal discard"

                  if blkdiscard "$_dev"
                  then
                      echo "[$_dev] Normal discard finished"
                  else
                      echo "[$_dev] Normal discard failed"
                  fi
              fi
          fi
      }

      # For paranoia's sake, try to discard all blocks on the remaining
      # top-level block devices in parallel.
      job_info "Attempt to discard blocks in parallel"
      for bdev in /sys/block/*; do
        discard $bdev &
      done

      wait

      # reevaluate the partitions after the wiping
      partprobe || :
