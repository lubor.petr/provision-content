---
Name: empty-gpt-tables
Description: Simple GPT partition table wipe of ALL disks on system.
Meta:
  color: orange
  feature-flags: sane-exit-codes
  icon: erase
  title: Digital Rebar Community Content
  type: erase
Documentation: |
  For every disk found in the system, write an empty GPT table
  on the disk.  This typically should follow the `prep-install`
  Stage which wipes the disks clean.

  Useful for disks destined for VMware vSphere VSAN or other storage
  solutions that require starting with a clean disk with an empty
  GPT table to use the disk.

  This task requires that the Param `zero-hard-disks-for-os-install` is
  set to `true` to operate, otherwise, it will not perform any GPT
  table changes.  The default value for this safety control Param is `false`.

  !!! warning
      This Task will WIPE ALL DISKS on a system if the Param
      `zero-hard-disks-for-os-install` is set to `true`.

RequiredParams:
  - zero-hard-disks-for-os-install
OptionalParams: []
Prerequisites: []
Templates:
  - Name: erase-disks
    Contents: |
      #!/usr/bin/env bash
      # Force create each disk with a blank GPT table

      PATH="$PATH:/usr/sbin:/usr/local/bin"

      set -e
      function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

      {{ if not ( .Param "zero-hard-disks-for-os-install" ) -}}
      echo ">>> NOTICE - not emptying GPT tables as Param 'zero-hard-disks-for-os-install'"
      echo ">>>          is NOT set to 'true'."
      exit 0
      {{ end -}}

      # filter out the most obvious device types, and hope we don't get anything
      # we shouldn't - this may need amending in the future - or better yet, a
      # param to control the list ... for complete device major number mappings,
      # see:  https://www.kernel.org/doc/Documentation/admin-guide/devices.txt

      DISKS=$(lsblk -e 7,9,11,12,21,22 -nd -o NAME)

      which sgdisk > /dev/null 2>&1 || xiterr 1 "Unable to find 'sgdisk'."

      echo ">>> Perform empty operation on the following discovered disks:"
      echo "    $DISKS" | tr '\n' ' '
      echo ""

      for DISK in $DISKS
      do
        echo ""
        echo ">>> Zapping and Emptying GPT partition table for disk:  '/dev/$DISK'"
        sgdisk -Z /dev/$DISK
        sgdisk -o /dev/$DISK
      done
