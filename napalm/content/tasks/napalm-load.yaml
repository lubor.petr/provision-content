---
Name: "napalm-load"
Description: "NAPALM switch merge or replace config task"
Documentation: |
  Runs the `load.py` script with either `merge` or `replace` actions,
  and a config file to either merge or replace the config with.

  Configuration for the `merge` or `replace` operation must be provided
  as a Template in the system.  Then in the `napalm/config-template` Param,
  define the name of the template which will be used in the operation.

  See other Params documentation for further help.

Meta:
  color: pink
  feature-flags: sane-exit-codes
  icon: tasks
  title: Digital Rebar
Prerequisites: []
RequiredParams:
  - "napalm/load-operation"
  - "napalm/config-template"
  - "napalm/commit-config"
  - "napalm/config-username"
  - "napalm/config-password"
  - "napalm/config-driver"
  - "napalm/config-hostname"
OptionalParams:
  - "napalm/config-extra"
  - "napalm/config-options"
Templates:
  - ID: "napalm-load.py.tmpl"
    Name: "napalm-load.py"
    Path: "napalm-load.py"
  - Name: "napalm-load.sh"
    Contents: |
      #!/usr/bin/env bash
      # Run the NAPALM load.py automation for Arista switches

      xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }
      {{ if ( .Param "rs-debug-enable" ) }}set -x{{ else }}set +x{{ end }}

      OP='{{ .Param "napalm/load-operation" }}'
      {{ $conf := "" }}
      {{ if .Param "napalm/config-template" -}}
      {{ $conf = ( .Param "napalm/config-template" ) -}}
      echo ">>> Using configuration from template '{{ $conf }}'"
      {{ else -}}
      xiterr 1 "No configuration was defined in 'napalm/config-template' Param."
      {{ end -}}

      cat <<'EOCONFIG' > ./load.conf
      {{ .CallTemplate $conf . }}
      EOCONFIG

      python3 ./napalm-load.py $OP ./load.conf
