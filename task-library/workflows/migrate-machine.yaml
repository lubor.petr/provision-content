---
Name: migrate-machine
Description: Workflow to migrate machine to new DRP endpoint.
Documentation: |
  This workflow will migrate a machine from one endpoint to another.

  While the task attempts to verify both endpoints are correctly setup before migration, it is important that contexts are setup and configured.  Specifically the `drpcli-runner` and `ansible-runner` contexts should be available and work on both endpoints.

  The following parameters are required:

    * `migrate-machine/new-drp-server-api-url`: URL to the endpoint (https://localhost:8092 as an example.)
    * `migrate-machine/new-drp-server-token`: A token for the new endpoint that can create a machine migrateTicket.

  The following parameters are optional:

    * `migrate-machine/skip-machine-migration`: If set to `true`, the task to move the machine object to the new endpoint will be skipped.  This is primarily for machines that might have been manually migrated, but still need the agent updated to the new endpoint.
    * `migrate-machine/skip-machine-destroy`: If set to `true`, the stub machine object will be left on the old endpoint after migration.  The default behavior is to leave the object on the old endpoint.
    * `migrate-machine/old-drp-server-token`: A token for the old endpoint that is needed to destroy the machine object left on the old endpoint.

  The following parameters should not be touched and are for internal processing:

    * `migrate-machine/esxi-ssh-priv-key`:
    * `migrate-machine/esxi-ssh-pub-key`: 

  Because ESXi does not have the same drpcli-based agent, the workflow cannot leverage all of the features available for other operating systems. It is required that sshd is enabled and keys are configured to access root on the machine being migrated.  If the agent is running on the machine, the workflow will attempt to start sshd, copy over the existing configuration if it exists, then place a temporary key.  Once migrated, the ssh service would be cleaned up, if already enabled and configured, the backed up keys will be restored and the temporary key removed.  If the ssh service wasn't started originally, it will be stopped and disabled.

Stages:
  - migrate-machine
  - complete-nobootenv
Meta:
  color: red
  icon: truck
