---
Name: ansible-playbooks-local
Description: A task to invoke a specific set of ansible playbooks pulled from git
Documentation: |
  !!! warning
      DEPRECATED: Use the `ansible-playbooks` task instead.  It matches the `ansible-apply` style.

  A task to invoke a specific set of ansible playbooks pulled from git.

  Sequence of operations (loops over all entries:

    1. collect args if provided
    1. git clone repo to name
    1. git checkout commit if provided
    1. cd to name & path
    1. run ansible-playbook playbook and args if provided
    1. remove the directories

  !!! note
      Requires Param `ansible/playbooks` - List of playbook git repos
      to run on the local machine.

Meta:
  color: black
  feature-flags: sane-exit-codes
  icon: "paper plane outline"
  title: RackN Content
RequiredParams:
  - ansible/playbooks
Templates:
  - Name: git-ansible-playbook
    Contents: |
      #!/usr/bin/env bash
      # Invoke a set of ansible playbooks pulled from git
      #

      {{template "setup.tmpl" .}}

      PKGS=""
      for WANT in git ansible
      do
        which $WANT > /dev/null 2>&1 || PKGS="${WANT} "
      done

      if [[ -n "$PKGS" ]]; then
          echo "Attempting to install missing packages:  $PKGS"
          install $PKGS
      fi

      {{ if .Param "ansible/connection-local"}}
      echo "LOCAL CONNECTION by ansible/connection-local: true"
      {{ else }}
      echo "WARNING: Was expecting ansible/connection-local: true, but it was false."
      {{ end }}

      echo "Running local ansible/playbooks"
      {{range $index, $playbook := .Param "ansible/playbooks"}}

          echo "======== Item {{$index}} of ansible/playbooks ========="
          NAME="{{if $playbook.name}}{{$playbook.name}}{{else}}playbook{{$index}}{{end}}"
          DIR="/{{if $playbook.path}}{{$playbook.path}}{{else}}{{end}}"
          PB="{{if $playbook.playbook}}{{$playbook.playbook}}{{else}}playbook{{$index}}.yaml{{end}}"

          {{if $playbook.template }}
            echo "  Creating playbook ${NAME}/${PB} from DRP template: {{$playbook.template}}"
            mkdir ${NAME}
            tee ${NAME}/${PB} >/dev/null << EOF
      {{$.CallTemplate $playbook.template $}}
      EOF
          {{else}}
            {{ if not $playbook.repo}}
              echo "ERROR: template or repo must be defined!"
              exit 1
            {{else}}
              echo "  Cloning the git repo: {{$playbook.repo}}"
              git clone "{{$playbook.repo}}" "$NAME"
              {{if not $playbook.commit }}
                echo "  No commit requsted, using default commit"
              {{else}}
                echo "  Checking out {{$playbook.commit}}"
                git checkout {{$playbook.commit}}
              {{end}}
            {{end}}
          {{end}}

          {{if not $playbook.data }}
              echo "  No special args requsted"
              ARGS=""
          {{else}}
              echo "  Using args: {{$playbook.args}}"
              ARGS="{{$playbook.args}}"
          {{end}}


          cd "${NAME}${DIR}"

          echo "Making DRP machine JSON available to playbook"
          tee digitalrebar.json >/dev/null << EOF
      { digitalrebar: $(drpcli machines show $RS_UUID)
      {{- if $playbook.extravars -}},
        {{$.CallTemplate $playbook.extravars $}}
      {{ end -}}
      }
      EOF

          echo "  Running the playbook $PB in $(pwd)"
          ansible-playbook \
            --connection=local \
            -i localhost, \
            $ARGS {{if not $playbook.verbosity}}no_log: True{{end}} \
            --extra-vars @digitalrebar.json \
            "$PB"

          cd -
      {{else}}
          echo "NOTE: No ansible/playbooks defined."
      {{end}}
      echo "======== End of Loop ========="

      echo "done"
      exit 0
