---
Name: "broker-context-manipulate"
Description: "Task to allocate/deallocate machines from a contexts"
Documentation: |
  This task will use parameters to drive the machine construction.

ExtraClaims:
  - scope: "machines"
    action: "*"
    specific: "*"
  - scope: "profiles"
    action: "*"
    specific: "*"
OptionalParams:
  - broker/set-icon
  - broker/set-color
  - broker/set-workflow
  - broker/set-pipeline
  - broker/set-context
  - cluster/machines
  - cluster/machine-types
  - cluster/profile
  - context-broker/tag
  - context-broker/tags
Meta:
  type: "configure"
  icon: "object group"
  color: "blue"
  title: "RackN Content"
  feature-flags: "sane-exit-codes"
Templates:
  - Name: "broker-context-manipulate"
    Contents: |
      #!/bin/bash
      # RackN Copyright 2021

      {{template "setup.tmpl" .}}

      # This is both the cluster profile name and the cluster machine name
      {{ $cluster_profile := .Param "cluster/profile"}}
      CLUSTER_PROFILE={{$cluster_profile}}
      echo "Operating on behalf of Cluster $CLUSTER_PROFILE: ux://clusters/Name:$CLUSTER_PROFILE"

      {{ $broker := .Machine.Name }}
      echo "Using Broker {{$broker}}: ux://resource_brokers/Name:{{$broker}}"

      list=$(drpcli machines list broker/name Eq {{$broker}} Profiles Eq $CLUSTER_PROFILE sort Name | jq '.[].Name' -r)
      echo "Current machines: $list"

      {{ if .Param "cluster/destroy" }}
      echo "Param cluster/destroy is true, starting cleanup sequence"
      while read mach ; do
          if [[ "$mach" == "" ]] ; then
              continue
          fi
          echo "  removing: ux://machines/Name:$mach"
          drpcli machines cleanup Name:$mach >/dev/null
      done <<< $list

      echo "Wait for machines to be removed..."
      while read mach ; do
        if [[ "$mach" == "" ]] ; then
          continue
        fi
        while drpcli machines exists Name:$mach 2>/dev/null >/dev/null ; do
          echo "Waiting to complete: ux://machines/Name:$mach" 
          drpcli machines wait Name:$mach WorkflowComplete true || :
        done
        echo "Removed: ux://machines/Name:$mach"
      done <<< $list

      echo "done - cluster destroy"
      exit 0
      {{ end }}

      echo "Param cluster/destroy is false, starting allocation sequence"
      {{if .ParamExists "cluster/machines" }}
      echo > ml.list
      {{  range $k, $v := .ParamExpand "cluster/machines" }}
      {{    $names := $.ObjectExpand $v.names }}
      {{    range $name := $names }}
      echo "{{$name}}" >> ml.list
      {{    end }}
      {{   end }}

      sort ml.list > new.list
      echo "$list" > old.list

      echo "------ Expected Machines list ------"
      cat new.list
      
      echo "------ Current Machines list ------"
      cat old.list
      echo "===== Diffing lists ======"
      diff -U0 old.list new.list | grep -v "\--- " | grep -v "+++ " | grep -v "@@ " > diff.out || :

      grep "^+" diff.out | sed 's/+//' > new.list || :
      grep "^-" diff.out | sed 's/-//' > remove.list || :
      remove=$(cat remove.list)

      echo "------ Add Machines list ------"
      cat new.list

      echo "------ Remove Machines list ------"
      echo "$remove"

      echo "====== PROCESS CHANGES ======"
      {{ if .ParamExists "context-broker/tag" }}
      values=( $(drpcli machines list "context-broker/tags=In({{.ParamExpand "context-broker/tag"}})" "context-broker/member-name=Re(.*)" | jq '.[].Name') )
      INDEX=0
      MAX=${#values[@]}
      {{ else }}
      values=()
      INDEX=0
      MAX=0
      {{ end }}

      echo "Adding Machines:"
      {{  range $k, $v := .ParamExpand "cluster/machines" }}
      {{    $workflow := $.Param "broker/set-workflow" }}
      {{    $pipeline := $.Param "broker/set-pipeline" }}
      {{    $icon := $.Param "broker/set-icon" }}
      {{    $color := $.Param "broker/set-color" }}
      {{    $tags := list ($.Param "cluster/profile") }}
      {{    if $v.workflow }}
      {{      $workflow = $v.workflow }}
      {{    end }}
      {{    if $v.pipeline }}
      {{          $pipeline = $v.pipeline }}
      {{    end }}
      {{    if $v.icon }}
      {{          $icon = $v.icon }}
      {{    end }}
      {{    if $v.color }}
      {{          $color = $v.color }}
      {{    end }}
      {{    if $v.tags }}
      {{          $tags = concat $tags $v.tags }}
      {{    end }}
      {{    $names := $.ObjectExpand $v.names }}
      {{    $workflow = $.ObjectExpand $workflow }}
      {{    $pipeline = $.ObjectExpand $pipeline }}
      {{    $icon = $.ObjectExpand $icon }}
      {{    $color = $.ObjectExpand $color }}
      {{    $tags = $.ObjectExpand $tags }}
      {{    range $name := $names }}
      mach="{{$name}}"
      if grep -q "^$mach\$" new.list ; then
        OWNER=""
        if [[ "$MAX" != "0" ]] ; then
          INDEX=$((INDEX + 1))
          INDEX=$((INDEX % MAX))
          OWNER="  docker-context/owner: ${values[$INDEX]}"
        fi
        cat > mach.yaml <<EOF
      ---
      Name: $mach
      Workflow: {{$workflow}}
      Bootenv: local
      Profiles:
        - {{$pipeline}}
        - {{$cluster_profile}}
      Params:
        cluster/tags: [ {{join "," $tags}} ]
        cluster/name: {{$cluster_profile}}
        broker/name: {{$broker}}
        broker/mach-name: $mach
      ${OWNER}
      Context: {{$.Param "broker/set-context"}}
      Meta:
        BaseContext: {{$.Param "broker/set-context"}}
        icon: {{$icon}}
        color: {{$color}}
      EOF
        echo "  creating machine: ux://machines/Name:$mach"
        drpcli machines create mach.yaml > out.json
        [[ $RS_DEBUG_ENABLE ]] && cat out.json
        echo "  created machine: ux://machines/$(jq -r '.[0].Uuid' out.json)"
        rm -f mach.yaml out.json
      fi
      {{    end }}
      {{  end}}
      {{ else }}
        remove=$list
      {{end}}

      echo "Removing $(wc -w <<< "$remove") machines"
      while read mach ; do
        if [[ "$mach" == "" ]] ; then
          continue
        fi
        echo "  cleaning up: ux://machines/Name:$mach"
        drpcli machines cleanup Name:$mach > out.json
        [[ RS_DEBUG_ENABLE ]] && cat out.json
        rm -f out.json
      done <<< $remove

      echo "Wait for machines to be removed..."
      while read mach ; do
        if [[ "$mach" == "" ]] ; then
          continue
        fi
        while drpcli machines exists Name:$mach 2>/dev/null >/dev/null ; do
          echo "Waiting to complete: ux://machines/Name:$mach"
          drpcli machines wait Name:$mach WorkflowComplete true || :
        done
        echo "  removed $mach"
      done <<< $remove

      rm -f diff.out new.list old.list ml.list remove.list
      echo "done"
      exit 0

