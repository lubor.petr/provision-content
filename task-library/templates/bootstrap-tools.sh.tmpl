#!/usr/bin/env bash
# installs packages defined by 'boostrap-tools' array on a DRP Endpoint
# RackN Copyright 2021

set -e
{{template "setup.tmpl" .}}

# set of packages to install (space separated), package names may need
# to be overridden if these are incorrect for a given Linux distro

{{ if ( .ParamExists "bootstrap-tools" ) -}}
echo ">>> Bootstrap tools Param contains a package list, starting processing."
{{ else -}}
echo ">>> There are not bootstrap tools listed in Param 'bootstrap-tools'.  Exiting."
exit 0
{{ end -}}

###
#  Package list is built with the ComposeParam template construct, so
#  it's an aggregate list of all occurences of the values on the
#  system for the Machine.
###
{{ $pkgs := ( .ComposeParam "bootstrap-tools" ) | join " " -}}
PKGS="{{ $pkgs }}"
echo ">>> Package list was composed to the following packages:"
echo "    $PKGS"
FAMILY=$(grep "^ID=" /etc/os-release | tr -d '"' | cut -d '=' -f 2)

# figure out if we need to try and use 'sudo' to run package installers
if which sudo > /dev/null 2>&1 ; then
  ID=$(id -u)
  [[ "$ID" == "0" ]] && SUDO="" || SUDO="sudo"
fi

echo ">>> Beginning package install process"
case $FAMILY in
  rhel|redhat|centos|photon|amzn|fedora|rocky|alma|almalinux)
    if which tdnf > /dev/null 2>&1 ; then
      YUM="tdnf"
    elif which dnf > /dev/null 2>&1 ; then
      YUM="dnf"
    elif which yum > /dev/null 2>&1 ; then
      YUM="yum"
    else
      echo "No 'tdnf', 'dnf', or 'yum' found on '$FAMILY' OS."
      echo "Unable to install '$PKGS'."
      exit 1
    fi
    if_update_needed $SUDO $YUM -y makecache
    $SUDO $YUM -y install $PKGS
    ;;
  debian|ubuntu) . /etc/os-release
    if which apt-get > /dev/null 2>&1 ; then
      APT="apt-get"
    elif which apt > /dev/null 2>&1 ; then
      APT="apt"
    else
      echo "No 'apt-get' or 'apt' found on '$FAMILY' OS."
      echo "Unable to install '$PKGS'."
      exit 1
    fi
    if_update_needed $SUDO $APT -y update
    $SUDO $APT -y install $PKGS
    ;;
  alpine)
    if_update_needed $SUDO apk update
    $SUDO apk add $PKGS
    ;;
  *) >&2 echo "Unsupported package manager family '$FAMILY'."
     exit 1
    ;;
esac
