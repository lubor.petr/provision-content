---
Name: "cloud/meta-apis"
Description: "Mapping for multiple Cloud Meta APIs"
Documentation: |
  Used by Task 'cloud-inventory' and 'cloud-detect-meta-api' tasks.

  Keys:

    * clouds is the ordered list of clouds to test during `cloud-detect-meta-api`
    * id is the field to use for the instance-id
    * apis is a dictionary per cloud that maps params to bash commands to retrieve the param

  For each apis entry:

    * `cloud/instance-id` (required):
    * `cloud/instance-type` (optional):
    * `cloud/placement/availability-zone` (optional):
    * `cloud/public-ipv4` (optional):

  apis entries are bash commands that will yield the expected value
  when evalated at `VAR=$([api command])`

Schema:
  type: "object"
  default:
    clouds:
      - linode
      - google
      - azure
      - oracle
      - digitalocean
      - aws
    id: "cloud/instance-id"
    apis:
      aws:
        "cloud/instance-id": "curl --max-time 20 -sfL http://169.254.169.254/latest/meta-data/instance-id"
        "cloud/instance-type": "curl -sfL http://169.254.169.254/latest/meta-data/instance-type"
        "cloud/placement/availability-zone": "curl -sfL http://169.254.169.254/latest/meta-data/placement/availability-zone"
        "cloud/public-hostname": "curl -sfL http://169.254.169.254/latest/meta-data/public-hostname"
        "cloud/public-ipv4": "curl -sfL http://169.254.169.254/latest/meta-data/public-ipv4"
        "cloud/private-ipv4": "curl -sfL http://169.254.169.254/latest/meta-data/local-ipv4"
      azure:
        "cloud/instance-id": 'curl --max-time 20 -sfL -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute/vmId?api-version=2021-05-01&format=text"'
        "cloud/instance-type": 'curl -sfL -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute/vmSize?api-version=2021-05-01&format=text"'
        "cloud/placement/availability-zone": 'curl -sfL -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute/location?api-version=2021-05-01&format=text"'
        "cloud/public-ipv4": 'curl -sfL -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2021-05-01&format=text"'
      digitalocean:
        "cloud/instance-id": "curl --max-time 20 -sfL curl http://169.254.169.254/metadata/v1/id"
        "cloud/public-ipv4": "curl -sfL curl http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address"
        "cloud/placement/availability-zone": "curl -sfL curl http://169.254.169.254/metadata/v1/region"
      google:
        "cloud/instance-id": 'curl --max-time 20 -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/id'
        "cloud/instance-type": 'curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/machine-type | awk "{n=split(\$1,a,\"/\"); print a[n]}"'
        "cloud/placement/availability-zone": 'curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/zone | awk "{n=split(\$1,a,\"/\"); print a[n]}"'
        "cloud/public-ipv4": 'curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip'
      oracle:
        "cloud/instance-id": 'curl --max-time 20 -sfL -H "Authorization: Bearer Oracle" -L http://169.254.169.254/opc/v2/instance/id'
        "cloud/instance-type": 'curl -sfL -H "Authorization: Bearer Oracle" -L http://169.254.169.254/opc/v2/instance/shape'
        "cloud/placement/availability-zone": 'curl -sfL -H "Authorization: Bearer Oracle" -L http://169.254.169.254/opc/v2/instance/region'
      linode:
        "cloud/instance-id": "cat /run/cloud-init/instance-data.json | jq -cr .instance_id"
        "cloud/instance-type": "cat /run/cloud-init/instance-data.json | jq -cr .meta_data.instance_type"
        "cloud/placement/availability-zone": "cat /run/cloud-init/instance-data.json | jq -cr .region"
  items:
    required:
      - clouds
      - id
      - apis
    properties:
      clouds:
        type: array
        items:
          type: string
      id:
        type: string
        default: "cloud/instance-id"
      apis:
        type: object
        items:
          properties:
            "cloud/instance-id":
              type: string
            "cloud/instance-type":
              type: string
            "cloud/placement/availability-zone":
              type: string
            "cloud/public-ipv4":
              type: string

Meta:
  icon: "cloud"
  color: "black"
  title: "Digital Rebar Community Content"
  copyright: "RackN 2022"
