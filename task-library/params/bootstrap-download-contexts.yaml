---
Name: "bootstrap-download-contexts"
Description: "Defines a list of contexts to download during bootstrap"
Documentation: |
  This is an array of strings where each string is a context that should be
  downloaded / updated during the bootstrap-context task.

  This is used by the bootstrapping system to add context images to the DRP Endpoint.

  By default, no contexts are specified.  If the operator sets this Param on the
  self-runner Machine object (either directly or via a Profile), then runs one
  of the bootstrap workflows, the context images will be installed.

  This parameter is Composable and Expandable.  This allows other profiles to incrementally
  and dynamically add to the list.

  An example workflow is `universal-bootstrap`.

  Example setting in YAML:

    ```yaml
    bootstrap-download-contexts:
      - drpcli-runner
      - context2
    ```

  Or in JSON:

    ```json
    { "bootstrap-download-contexts": [ "drpcli-runner", "context2" ] }
    ```

Meta:
  icon: "cog"
  color: "blue"
  title: "Digital Rebar Community Connect"
  copyright: "RackN 2022"

Schema:
  type: "array"
  items:
    type: "string"
