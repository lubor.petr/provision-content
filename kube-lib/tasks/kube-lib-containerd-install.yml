---
Description: "Install containerd"
Name: "kube-lib-containerd-install"
Documentation: |
  Installs containerd cni from the binary version

  NO TUNING PROVIDED AT THIS TIME.

  Reference: <https://github.com/containerd/containerd/>

RequiredParams:
  - kube-lib/containerd-version
  - kube-lib/crictl-version
ExtraClaims:
  - scope: "files"
    action: "*"
    specific: "*"
Templates:
  - Name: "kubelet.conf"
    Path: "/etc/systemd/system/kubelet.service.d/0-containerd.conf"
    Meta:
      OS: linux
    Contents: |
      [Service]
      Environment="KUBELET_EXTRA_ARGS=--container-runtime=remote --runtime-request-timeout=15m --container-runtime-endpoint=unix:///run/containerd/containerd.sock"
  - Name: "crictl.yaml"
    Path: "/etc/crictl.yaml"
    Meta:
      OS: linux
    Contents: |
      runtime-endpoint: unix:///run/containerd/containerd.sock
      image-endpoint: unix:///run/containerd/containerd.sock
      timeout: 10
  - Name: "containerd.conf"
    Path: "/etc/modules-load.d/containerd.conf"
    Meta:
      OS: linux
    Contents: |
      overlay
      br_netfilter
  - Name: "containerd.service"
    Path: "/etc/systemd/system/containerd.service"
    Meta:
      OS: linux
    Contents: |
      # Copyright The containerd Authors.
      #
      # Licensed under the Apache License, Version 2.0 (the "License");
      # you may not use this file except in compliance with the License.
      # You may obtain a copy of the License at
      #
      #     http://www.apache.org/licenses/LICENSE-2.0
      #
      # Unless required by applicable law or agreed to in writing, software
      # distributed under the License is distributed on an "AS IS" BASIS,
      # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      # See the License for the specific language governing permissions and
      # limitations under the License.

      [Unit]
      Description=containerd container runtime
      Documentation=https://containerd.io
      After=network.target local-fs.target

      [Service]
      ExecStartPre=-/sbin/modprobe overlay
      ExecStart=/usr/local/bin/containerd

      Type=notify
      Delegate=yes
      KillMode=process
      Restart=always
      RestartSec=5
      # Having non-zero Limit*s causes performance problems due to accounting overhead
      # in the kernel. We recommend using cgroups to do container-local accounting.
      LimitNPROC=infinity
      LimitCORE=infinity
      LimitNOFILE=infinity
      # Comment TasksMax if your systemd version does not supports it.
      # Only systemd 226 and above support this version.
      TasksMax=infinity
      OOMScoreAdjust=-999

      [Install]
      WantedBy=multi-user.target
  - Name: "containerd-install.sh"
    Meta:
      OS: linux
    Path: ""
    Contents: |
      #!/usr/bin/env bash

      {{ if eq (len .Machine.Context) 0 }}
      {{ template "setup.tmpl" . }}

      {{ template "download-tools.tmpl" .}}

      echo "install prereqs: socat, conntrack-tools, tc, tar and libseccomp"
      case $osfamily in
        debian|ubuntu) install socat iproute2 conntrack-tools tar libseccomp2;;
        *) install tar socat conntrack-tools tar iproute-tc libseccomp;;
      esac

      OS={{if .Machine.OS}}{{ .Machine.OS }}{{else}}"linux"{{end}}
      ARCH="{{ .Machine.Arch }}"

      echo "Installing containerd"
      if which containerd; then
        echo "  found containerd, restart and nothing else..."
        systemctl daemon-reload
        sudo systemctl restart containerd
      else
        {{ $cachesource := "kube-lib/containerd-version" }}
        {{ $arch := list "linux" .Machine.Arch | join "-" }}
        {{ $file := .Param $cachesource }}
        {{ $source := get $file $arch }}
        drp_cache "{{ get $file "name" }}" "{{ get $file "version" }}" "$OS" "$ARCH" "{{ get $source "url" }}" "{{ get $source "sha256sum" }}" "{{ get $source "explode" }}"

        echo "extracting to {{ get $file "name" }}"
        tar --no-overwrite-dir -C /usr/local/ -xzf "{{ get $file "name" }}"

        echo "{{.Machine.Address}} $(hostname -s) $(hostname)" >> /etc/hosts
        mkdir -p /etc/containerd/
        containerd config default > /etc/containerd/config.toml

        systemctl daemon-reload
        systemctl start containerd
        echo "  containerd installed as systemd"

      fi
      echo "Running containerd version $(containerd --version)"

      echo "Install CNI plugins"
      if [[ -d /opt/cni/bin ]] ; then
        echo "  found CNI directory, moving on..."
      else
        {{ $cachesource := "kube-lib/cni-plugins-version" }}
        {{ $arch := list "linux" .Machine.Arch | join "-" }}
        {{ $file := .Param $cachesource }}
        {{ $source := get $file $arch }}
        drp_cache "{{ get $file "name" }}" "{{ get $file "version" }}" "$OS" "$ARCH" "{{ get $source "url" }}" "{{ get $source "sha256sum" }}" "{{ get $source "explode" }}"
        mkdir -p /opt/cni/bin
        tar --no-overwrite-dir -C /opt/cni/bin -xzf "{{ get $file "name" }}"
      fi

      echo "Installing crictl"
      if which crictl ; then
        echo " found crictl, moving on..."
      else
        {{ $cachesource := "kube-lib/crictl-version" }}
        {{ $arch := list "linux" .Machine.Arch | join "-" }}
        {{ $file := .Param $cachesource }}
        {{ $source := get $file $arch }}
        drp_cache "{{ get $file "name" }}" "{{ get $file "version" }}" "$OS" "$ARCH" "{{ get $source "url" }}" "{{ get $source "sha256sum" }}" "{{ get $source "explode" }}"
        tar -C /usr/local/bin -zxzf "{{ get $file "name" }}"
        echo "  {{ get $file "name" }} installed to /usr/local/bin/"
      fi

      if [[ -f "/etc/modules-load.d/k8s.conf" ]] ; then
        echo "Do not update iptables, found /etc/modules-load.d/k8s.conf"
      else
        echo "Set iptables to see bridged traffic"
        cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
      overlay
      br_netfilter
      EOF

        echo "  Modprobe overlay and br_netfilter"
        sudo modprobe overlay
        sudo modprobe br_netfilter

        echo "  Add net.bridge.bridge-nf-call-* to k8s.conf"
        cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
      net.bridge.bridge-nf-call-ip6tables = 1
      net.ipv4.ip_forward                 = 1
      net.bridge.bridge-nf-call-iptables  = 1
      EOF
        echo " sysctl --system"
        sudo sysctl --system
      fi

      echo "Done"
      exit 0
      {{ else }}
      job_error "cannot install, running in a container"
      {{ end }}

Meta:
  icon: "boxes"
  color: "orange"
  title: "Community Content"
  copyright: "RackN 2022"
