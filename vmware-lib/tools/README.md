All script tools have been removed from the vmware-lib content.

Context containers are built and staged via the rackn/containers repository
now.  For Context container installation, please see the Documenation
Knowledge Base article:

https://docs.rackn.io/en/latest/doc/kb/kb-00080.html

