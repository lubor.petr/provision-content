---
Name: "vsphere-bootorder-change"
Description: "A simple task to run Powershell bootorder changes on VMs in vSphere"
Documentation: |
  This task is a nasty hack that runs Powershell against a vSphere service
  and modifies the Virtual Machine's boot order.  Why does VMware have to
  make this so hard?

  This Task has two modes for defining the Virtual machines to target:

    - dynamic - all DRP based Cluster managed Virtual machines (default mode)
    - list based - an explicit list defined by `vsphere/virtual-machines`

  Notes for each mode are below.

  Dynamic mode:

  Must be run from the Cluster, and the Resource Broker must be defined as
  "broker/name" on the Cluster.  Each Machine in the Cluster MUST have it's
  Cluster specific Profile attached to the Machine to identify it.

  In the Dynamic Cluster mode, the Cluster must be set up correctly, with the
  Cluster defined as Cluster.Meta.machine-role set to `cluster`.  The Cluster
  must have `broker/name` set directly or indirectly via Profiles, to determine
  the Resource Broker to operate against.

  The Resource Broker must have the Broker Profile attached to it to extract
  the vSphere Node, User, and Password connection details.

  Each Virtual Machine must also have the Cluster named Profile attached to the
  Machine to be able to identify that it belongs in the named Cluster.  This is
  how the Machines target list is dynamically determined.

  List Based mode:

  The operator must specify the Virtual Machines to target in the string list
  type Param named `vsphere/virtual-machines`.

  In addition, the connection details (vsphere service host, user, and password
  credentials) must be specified in teh `vsphere/server`, `vsphere/user`, and
  `vsphere/password` Params.

    !!! note
        Requires that the `task-library` Content bundle be installed (in the Catalog)
        for the Powershell/PowerCLI dependency requirements if needed in the
        context that the Task is running.

OptionalParams:
  - "vsphere/virtual-machines"
  - "vsphere/server"
  - "vsphere/user"
  - "vsphere/password"
ExtraClaims:
  - scope: "machines"
    action: "*"
    specific: "*"
  - scope: "profiles"
    action: "*"
    specific: "*"
Meta:
  icon: "terminal"
  color: "blue"
  title: "Digital Rebar Community Content"
  feature-flags: "sane-exit-codes"
Templates:
  - Name: "install-powershell.sh" 
    ID: "install-powershell.sh.tmpl"
  - Name: "install-powercli.sh" 
    ID: "install-powercli.sh.tmpl"
  - Name: "vsphere-bootorder-change.pwsh"
    ID: "vsphere-bootorder-change.pwsh.tmpl"
    Path: "./vsphere-bootorder-change.pwsh"
  - Name: "vsphere-bootorder-change.sh"
    Contents: |
      #!/usr/bin/env bash
      # Change boot order of VMs

      ###
      #   broker/name is defined on cluster - and Profile should contain the credentials
      #   .Machine.Name should be the Cluster name
      #   Param 'vsphere/drp-cluster' must also be set to the name of the cluster, to find
      #   the cluster details to operate on.
      ###

      ### setup.tmpl
      {{ template "setup.tmpl" . }}

      if ! which pwsh 2> /dev/null
      then
        job_error "Required 'pwsh' not found in path"
        exit 1
      fi

      PCLI=$(pwsh -NonInteractive -OutputFormat Text -Command 'Get-Module  -ListAvailable VMware.PowerCLI | Select Version | Out-String -Stream')
      if [[ -z "$PCLI" ]]
      then
        job_error "Required PowerCLI modules not found in PowerShell installation."
        exit 1
      fi

      ### govc-cluster-create.sh
      JQ=$(which jq)
      [[ -z "$JQ" ]] && JQ=$(which drpjq)    || true
      [[ -z "$JQ" ]] && JQ=$(which drpclijq) || true
      [[ -z "$JQ" ]] && JQ=$(which gojq)     || true
      if [[ -z "$JQ" ]]; then
        D="$(which drpcli)"
        if [[ -n "$D" ]]; then
          ln -s $D /usr/local/bin/drpjq
          JQ="/usr/local/bin/drpjq"
        else
          xiterr 1 "Unable to find 'jq' or alternative to use."
        fi
      fi

      # we must have our rendered Powershell script to execute, if it's been
      # rendered, chmod it so we can run it directly
      PWSH="./vsphere-bootorder-change.pwsh"

      if [[ -r "$PWSH" ]]
      then
        chmod +x $PWSH
      else
        job_error "'bootorder-change.pwsh' was not rendered for this task to run it"
        exit 1
      fi

      # if VMs are explicitly defined in vsphere/virtual-machines, use that, coupled
      # with the vsphere/connection-details to operate against, otherwise, assume we
      # are in "cluster" mode, and operate against all VMs defined on the Cluster
      # Machine object we are running the Tasks on
      {{ if ( .ParamExists "vsphere/virtual-machines" ) -}}
      MACHS="{{ .Param "vsphere/virtual-machines" | join " " }}"

      VSRV="{{ .Param "vsphere/server" }}"
      VUSR="{{ .Param "vsphere/user" }}"
      VPWD="{{ .Param "vsphere/password" }}"

      {{ else -}}

      # operate via the Cluster machine object, reference the Cluster Profile 
      # to get appropriate information, ncluding Broker, and other details
      
      ROLE="{{ get .Machine.Meta "machine-role" }}"

      if [[ "$ROLE" != "cluster" ]]
      then
        job_error "This task must be run on a Cluster type Machine, got role '$ROLE'"
        exit 1
      fi

      CLUSTER="{{ .Machine.Name }}"
      drpcli clusters exists Name:$CLUSTER 2> /dev/null
      (( $? )) && xiterr 1 "This does not appear to be a Cluster type machine"
      # make sure our Cluster Profile is on the Cluster Machine

      PROFS=$(drpcli clusters show Name:$CLUSTER --slim=Params,Meta | jq '.Profiles')

      # check if our Cluster Profile is on the Cluster Machine, if not
      # add it, though this might be bad to assume
      if ! echo $PROFS | grep -q '"'$CLUSTER'"'
      then
        drpcli clusters addprofile Name:$CLUSTER $CLUSTER
      fi

      # determine which is our Broker that has our server/user/pass info
      {{ if .ParamExists "broker/name" -}}
      BROKER="{{ .Param "broker/name" }}"
      {{ else -}}
      job_error "This machine does not appear to have 'broker/name' defined on it."
      echo ">>>  If this is a vCenter/VCSA instance operating against a Cluster, then the"
      echo "     specific Cluster Profile must also exist on this Context Machine to operate"
      echo "     correctly.  Exiting with error."
      exit 1
      {{ end -}}

      drpcli resource_brokers exists Name:$BROKER 2> /dev/null
      (( $? )) && xiterr 1 "'broker/name' does not appear to be a Resource Broker type machine"

      # get the machines in the Cluster
      MACHS=$(drpcli machines list Profiles in $CLUSTER Name Ne $CLUSTER | jq -r '.[].Name' | tr '\n' ' ')

      # collect our Broker details to execute the powershell against
      VPWD=$(drpcli profiles get $BROKER param "vsphere/password" --decode | jq -r '.')
      VSRV=$(drpcli profiles get $BROKER param "vsphere/server" | jq -r '.')
      VUSR=$(drpcli profiles get $BROKER param "vsphere/user" | jq -r '.')

      if [[ -z "$MACHS" ]]
      then
        job_info "No machines found for cluster '$CLUSTER'."
        exit 0
      fi
      {{ end -}}

      echo ">>> Beginning vSphere Boot Order change with following values:"
      echo ""
      [[ -n "$BROKER" ]] && echo "    Broker    :: $BROKER"
      [[ -n "$CLUSTER" ]] && echo "    Cluster   :: $CLUSTER"
      echo "    Machines  :: $MACHS"
      echo "    vSphere   :: $VSRV"
      echo "    User      :: $VUSR"
      echo "    Password  :: <...obfuscated...>"
      echo ""

      for MACH in $MACHS
      do
        echo ">>> Changing bootorder for '$MACH' on '$VSRV'"
        $PWSH "$MACH" "$VSRV" "$VUSR" "$VPWD"
      done
      
