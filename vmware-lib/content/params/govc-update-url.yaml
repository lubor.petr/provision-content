---
Name: "govc/update-url"
Description: "URL to download updated 'govc' binary from"
Documentation: |
  The URL to use to download an updated TGZ, TAR, or raw binary for
  `govc` from.  The URL can contain Golang template constructs which
  will be expanded during render time.

  Note that the default URL location is the GoVMOMI `govc` Binary default
  releases location.  If you're DRP Endpoint has internet access, you
  can just set the `govc/update-version` Param to a version string to
  get (eg `v0.30.4`); which is a part of the default URL value.

  Example of setting this to a non-default download location, but still
  using Golang template expansion with the Param `govc/update-version`:

  - `{{ .SecureProvisionerURL }}/files/artifacts/govc-{{ .Param "govc/update-version" }}.tgz`

  This could expand to something like (with `govc/update-version` set
  to `v0.30.4`):

  - `https://1.2.3.4:8090/files/artifacts/govc-v0.30.4.tgz`

  The default value is:

  - `https://github.com/vmware/govmomi/releases/download/{{ .Param "govc/update-version" }}/govc_Linux_x86_64.tar.gz`

Meta:
  color: "blue"
  icon: "globe"
  title: "RackN Content"
Schema:
  type: "string"
  default: 'https://github.com/vmware/govmomi/releases/download/{{ .Param "govc/update-version" }}/govc_Linux_x86_64.tar.gz'
