---
Name: "esxi/vsan-claim-capacity-group-size"
Description: "Defines possible max disks to use in each VSAN disk group on an ESXi host."
Documentation: |
  This param defines the possible maximum Capacity disks each VSAN group should be.
  This number should also be as closely evenly divisibly by the
  `esxi/vsan-claim-cache-count` param specification.  This allows to create groups
  that are as closely balanced with Cache to Capacity disk assignments.

  In the "simple" disk claiming ruleset, all Boot disks, VSAN, and VMFS claimed disks
  are filtered out.  The list of remaining disks are sorted by size (smallest first);
  and then the first disk (or disks, if more than one is specified in
  `esxi/vsan-claim-cahce-count`) in the list is claimed for Cache disk; the remainder
  are claimed for capacity disks.

  Note that VSAN can only utilize 1 Cache and 7 Capacity disks as a maximum, per
  VSAN disk group.  This Param is responsible for activating and building multiple
  VSAN disk groups.

  In the "simple" claiming ruleset, setting this value to "4" (for example) will select
  the first 4 disks in the Capacity filtered list.  If the `esxi/vsan-claim-cache-count`
  is subsequently set to 2 (two); then 2 VSAN Groups with 4 Capacity disks would be
  built.

  This Param must be an integer from 1 and NOTgreater than 7.  The 7 disk capacity
  limit per VSAN group is a VMware restriction.

  In addition - there can be no more than 5 VSAN disk groups which means that a
  single VSAN host can only support a maximum of 35 Capacity disks, which would
  require 5 Cache disks for a total of 40 VSAN claimed disks.

  Even if the maximum group size is 7; it is entirely likely that group sizes
  will be smaller, depending on the total number of disks available in the
  system.

  By default this Param specifies VSAN Groups of 7 capacity disks.

  !!! note
      For VSAN Witness disk claiming, this Param is ignored as only 1 Capacity
      disk can be claimed.

Meta:
  color: "green"
  icon: "database"
  title: "RackN Content"
Schema:
  type: "number"
  default: 7
  minimum: 1
  maximum: 7
