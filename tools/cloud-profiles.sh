#!/usr/bin/env bash
# RackN Copyright 2021
# Upload Local Cloud Credentials

export PATH=$PATH:$PWD

get_icon() {
  drpcli $ep profiles meta resource-$*-cloud | jq -r .icon
}

process_endpoint() {
  local s=$1
  local ep="-u $s"
  echo "2/3 Checking Endpoint $ep Prereqs"
  ep="-u $s"
  if [[ "$s" == "$drpid" ]]; then
    ep=""
  fi

  echo "Checking $ep license..."
  if drpcli $ep contents exists rackn-license ; then
    echo "  verified rackn-license is available"
  else
    if [[ -f rackn-license.json ]] ; then
      echo "  found local copy of rackn-license, uploading"
      drpcli $ep contents upload rackn-license.json > /dev/null 2>/dev/null
    else
      echo "MISSING: rackn-license.  Install using UX"
      exit 1
    fi
  fi

  if drpcli $ep contents exists universal ; then
    echo "  verified universal workflow is available"
  else
    echo "MISSING: universal workflow is required"
    exit 1
  fi

  if drpcli $ep contents exists cloud-wrappers ; then
    echo "  verified cloud-wrappers is available"
  else
    echo "  MISSING: missing cloud-wrappers content pack"
    exit 1
  fi

  echo "3/3 Building Resource Brokers for $s"
  if [[ -f ~/.aws/credentials ]]; then
    if drpcli $ep resource_brokers exists Name:aws-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping AWS, already exists"
    else
      echo "  upload AWS broker"
      drpcli $ep resource_brokers create - >/dev/null << EOF
---
Name: "aws-broker"
Description: "AWS Broker"
Workflow: universal-start
Profiles:
  - "universal-application-broker-base"
  - "resource-aws-cloud"
Params:
  "rsa/key-user": "ec2-user"
  "aws/secret-key": "$(awk '/aws_secret_access_key/{ print $3}' ~/.aws/credentials)"
  "aws/access-key-id": "$(awk '/aws_access_key_id/{ print $3}' ~/.aws/credentials)"
Meta:
  BaseContext: terraform
  color: "blue"
  icon: "$(get_icon aws)"
  title: "generated"
EOF
    fi
    if drpcli $ep contexts exists awscli-runner > /dev/null 2>/dev/null ; then
      echo "  Skipping awscli-runner context already exists"
    else
      echo "  Creating awscli-runner context"
      drpcli $ep contexts create - >/dev/null << EOF
---
Name: awscli-runner
Description: Run AWS CLI in Context
Engine: docker-context
Image: awscli-runner_v1.2.5
Meta:
  Checksum: ecf31daa4492f85635b01d0482f1976a4ceeba67151e9a1aea9426ef44d9d0aa
  color: "black"
  icon: "$(get_icon aws)"
  title: "generated"
EOF
    fi
    if drpcli $ep resource_brokers exists Name:aws-cli > /dev/null 2>/dev/null ; then
      echo "  Skipping aws-cli broker already exists"
    else
      echo "  Creating aws-cli broker"
      drpcli $ep resource_brokers create - >/dev/null << EOF
---
Name: "aws-cli"
Description: "AWSCLI Broker"
Workflow: universal-start
Profiles:
  - "universal-application-broker-base"
  - "resource-aws-cli"
Params:
  "rsa/key-user": "ec2-user"
  "aws/secret-key": "$(awk '/aws_secret_access_key/{ print $3}' ~/.aws/credentials)"
  "aws/access-key-id": "$(awk '/aws_access_key_id/{ print $3}' ~/.aws/credentials)"
Meta:
  BaseContext: awscli-runner
  color: "black"
  icon: "$(get_icon aws)"
  title: "generated"
EOF
    fi
  else
    echo "  no AWS credentials, skipping"
  fi

  if [[ -f ~/.oci/config ]]; then
    if drpcli $ep resource_brokers exists Name:oracle-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Oracle, already exists"
    else
      echo "  upload Oracle broker"

      public="$(cat $HOME/.oci/config | grep "key_file=" | awk '{split($0,a,"="); print a[2]}')"
      private="$(echo $public | sed "s/_public//g")"
      private="$(echo $private | sed "s/\~//g")"
      if [[ -f $HOME$private ]]; then
        drpcli $ep resource_brokers create - >/dev/null << EOF
{
  "Name": "oracle-broker",
  "Description": "Oracle Broker",
  "Workflow": "universal-start",
  "Profiles": ["universal-application-broker-base","resource-oracle-cloud"],
  "Params": {
    "rsa/key-user": "opc",
    "oracle/user-ocid": "$(cat ~/.oci/config | grep "user=" | awk '{split($0,a,"="); print a[2]}')",
    "oracle/tenancy-ocid": "$(cat ~/.oci/config | grep "tenancy=" | awk '{split($0,a,"="); print a[2]}')",
    "oracle/fingerprint": "$(cat ~/.oci/config | grep "fingerprint=" | awk '{split($0,a,"="); print a[2]}')",
    "oracle/region": "$(cat ~/.oci/config | grep "region=" | awk '{split($0,a,"="); print a[2]}')",
    "oracle/compartment-id": "$(cat ~/.oci/config | grep "tenancy=" | awk '{split($0,a,"="); print a[2]}')",
    "oracle/private-key": "$(cat $HOME$private)",
  },
  "Meta": {
    "BaseContext": "terraform",
    "color": "blue",
    "icon": "$(get_icon oracle)",
    "title": "generated",
  }
}
EOF
      else
        echo "  SKIPPING: could not find Oracle private key $private"
      fi
    fi
  else
    echo "  no ORACLE credentials, skipping"
  fi

  # upload aws & google credentials
  google=$(ls ~/.gconf/desktop/*.json 2>/dev/null || echo "none")
  if [[ -f $google ]]; then
    if drpcli $ep resource_brokers exists Name:google-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Google, already exists"
    else
        echo "  upload Google broker"
        gconf=$(cat $google) > /dev/null
        drpcli $ep resource_brokers create - >/dev/null << EOF
{
  "Name": "google-broker",
  "Description": "GCE Broker",
  "Workflow": "universal-start",
  "Profiles": [ "universal-application-broker-base","resource-google-cloud" ],
  "Params": {
    "google/project-id": "$(jq -r .project_id <<< "$gconf")",
    "rsa/key-user": "rob",
    "google/credential": $(cat $google),
  },
  "Meta": {
    "BaseContext": "terraform",
    "color": "blue",
    "icon": "$(get_icon google)",
    "title": "generated",
  }
}
EOF
    fi
  else
    echo "  no Google credentials, skipping"
  fi

  if [[ "$DIGITALOCEAN_TOKEN" == "" ]] ; then
      if [[ -f ~/.digitalocean/credentials ]]; then
          export DIGITALOCEAN_TOKEN=$(cat ~/.digitalocean/credentials)
      fi
  fi
  if [[ $DIGITALOCEAN_TOKEN ]]; then
    if drpcli $ep resource_brokers exists Name:digitalocean-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Digital Ocean, already exists"
    else
      echo "  upload Digital Ocean broker"
        drpcli $ep resource_brokers create - >/dev/null << EOF
---
Name: "digitalocean-broker"
Description: "Digital Ocean Broker"
Workflow: universal-start
Profiles:
  - "universal-application-broker-base"
  - "resource-digitalocean-cloud"
Params:
  "digitalocean/token": "$DIGITALOCEAN_TOKEN"
Meta:
  BaseContext: terraform
  color: "blue"
  icon: "$(get_icon digitalocean)"
  title: "generated"
EOF
    fi
  else
    echo "  Skipping Digital Ocean, no token DIGITALOCEAN_TOKEN"
  fi

  if [[ "$LINODE_TOKEN" == "" ]] ; then
      if [[ -f ~/.linode/credentials ]]; then
          export LINODE_TOKEN=$(cat ~/.linode/credentials)
      fi
  fi
  if [[ $LINODE_TOKEN ]]; then
    if drpcli $ep resource_brokers exists Name:linode-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Linode, already exists"
    else
      echo "  upload linode broker"
      drpcli $ep resource_brokers create - >/dev/null << EOF
---
Name: "linode-broker"
Description: "Linode Broker"
Workflow: universal-start
Profiles:
  - "universal-application-broker-base"
  - "resource-linode-cloud"
Params:
  "linode/instance-image": "linode/centos-stream9"
  "linode/instance-type": "g6-standard-2"
  "linode/root-password": "r0cket^^^k8ts"
  "linode/token": "$LINODE_TOKEN"
Meta:
  BaseContext: terraform
  color: "blue"
  icon: "$(get_icon linode)"
  title: "generated"
EOF
    fi
  else
    echo "  Skipping Linode, no token LINODE_TOKEN"
  fi

  if [[ -f ~/.pnap/config.yaml ]]; then
    if drpcli $ep resource_brokers exists Name:pnap-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Phoenix NAP, already exists"
    else
      echo "  upload Phoenix NAP (pnap) broker"
      drpcli $ep resource_brokers create - >/dev/null << EOF
---
Name: "pnap-broker"
Description: "Phoenix NAP Credentials"
Workflow: universal-start
Profiles:
  - "universal-application-broker-base"
  - "resource-pnap-cloud"
Params:
  "cloud/provider": "pnap"
  "rsa/key-user": "ubuntu"
  "pnap/client-id": "$(cat ~/.pnap/config.yaml | grep "clientId:" | awk '{split($0,a,": "); print a[2]}')"
  "pnap/client-secret": "$(cat ~/.pnap/config.yaml | grep "clientSecret:" | awk '{split($0,a,": "); print a[2]}')"
Meta:
  BaseContext: terraform
  color: "blue"
  icon: "$(get_icon pnap)"
  title: "generated"
EOF
    fi
  else
    echo "  Skipping Phoenix NAP, no ~/.pnap/config.yaml"
  fi

  if which az > /dev/null ; then
    if drpcli $ep resource_brokers exists Name:azure-broker > /dev/null 2>/dev/null ; then
      echo "  Skipping Azure, already exists"
    else
      echo "upload Azure Broker"
      if az login > /dev/null ; then
        login=$(az account show)
        echo "  Azure login confirmed for user: $(jq -r '.user.name' <<< $login)"
        azure_subscription_id=$(jq -r .id <<< "$login")
        az account set --subscription="$azure_subscription_id" > /dev/null
        if az vm list > /dev/null ; then
          echo "  Azure login verified - You can ignore WARNINGS!"
          # see https://www.terraform.io/docs/providers/azurerm/guides/service_principal_client_secret.html
          azure_resource=$(az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/$azure_subscription_id")
          drpcli $ep resource_brokers create - >/dev/null << EOF
{
  "Name": "azure-broker",
  "Description": "Azure Broker",
  "Workflow": "universal-start",
  "Profiles": [ "universal-application-broker-base", "resource-azure-cloud" ],
  "Params": {
    "azure/subscription_id": "$azure_subscription_id",
    "azure/app_id": "$(jq -r .appId <<< "$azure_resource")",
    "azure/tenant": "$(jq -r .tenant <<< "$azure_resource")",
    "rsa/key-user": "rob",
    "azure/password": "$(jq -r .password <<< "$azure_resource")",
  },
  "Meta": {
    "BaseContext": "terraform",
    "color": "blue",
    "icon": "$(get_icon azure)",
    "title": "generated",
  }
}
EOF
        else
          echo "  WARNING: could not list vms to validate creditentials!"
        fi
      else
        echo "  WARNING: az login failed (did you see the web page pop-up?)"
      fi
    fi
  else
    echo "  Skipping Azure, no az cli installed"
  fi

  echo "  ----------------------------"
  runner=$(drpcli $ep machines list machine-self-runner=true | jq -rC '.[0].Uuid')
  echo "  rebootstrap DRP self-runner $runner (endpoint $s)"
  drpcli $ep machines update $runner '{"Locked":false}' > /dev/null
  drpcli $ep machines work_order add $runner rebootstrap-drp > /dev/null
  echo "Done with $s"
  echo
}

set -e

echo "RackN Digital Rebar Cloud Credentials Uploader (v2.1 Nov 2021)"
echo "=============================================================="
echo "1/3 Checking Cloudwapper Prereqs (applies to all endpoints)"

if ! which drpcli > /dev/null ; then
  echo "MISSING: drpcli required!"
  exit 1
else
  echo "  verified drpcli is installed"
fi

if ! which jq > /dev/null ; then
  echo "  adding jq via drpcli as soft link in current directory"
  ln -s $(which drpcli) jq >/dev/null
else
  echo "  verified jq is installed"
fi

if ! drpcli info get > /dev/null ; then
  echo "MISSING: set RS_ENDPOINT and RS_KEY!"
exit 1
else
  drpid=$(drpcli info get | jq -r .id | sed 's/:/-/g')
  echo "  verified drp $drpid access and credentials"
fi

echo
echo "UPDATING PRIMARY: $drpid"
echo
process_endpoint "$drpid"

if [[ $(drpcli endpoints count) -gt 0 ]]; then
  ENDPOINTS="$(drpcli endpoints list | jq -r .[].Id)"
  echo
  echo "UPDATING ENDPOINTS: $ENDPOINTS"
  echo

  for s in $ENDPOINTS; do
    echo "-------------------------------------------------------------"
    process_endpoint "$s"
  done
else
  echo "-------------------------------------------------------------"
  echo "Skipping Endpoints.  None Found"
fi
echo "=============================================================="
echo "All Done!"
