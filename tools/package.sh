#!/bin/bash

set -e

family=386
if [[ $(uname -m) == x86_64 ]] ; then
    family=amd64
fi
case $(uname -s) in
    Darwin)
        shasum="command shasum -a 256"
        ;;
    Linux)
        shasum="command sha256sum"
        ;;
    *)
        # Someday, support installing on Windows.  Service creation could be tricky.
        echo "No idea how to check sha256sums"
        exit 1;;
esac

P=`pwd`
PATH="$P/bin:$PATH"
mkdir -p bin
export GO111MODULE=on
[[ -x $P/bin/drpcli ]] || (go mod tidy ; go build -o $P/bin/drpcli tools/drpcli.go)

version=$(tools/version.sh)

DOC_VERSION=""
if [[ "$CI_COMMIT_BRANCH" != "" ]] ; then
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%%-*}
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%.*}
  DOC_VERSION=$CI_COMMIT_BRANCH
fi
if [[ "$CI_COMMIT_TAG" != "" ]] ; then
  CI_COMMIT_TAG=${CI_COMMIT_TAG%%-*}
  CI_COMMIT_TAG=${CI_COMMIT_TAG%.*}
  DOC_VERSION=$CI_COMMIT_TAG
fi
if [[ "$DOC_VERSION" == "v4" ]] ; then
  DOC_VERSION=tip
fi
if [[ "$VERSION" == "" ]] ; then
  DOC_VERSION=tip
fi

tools/pieces.sh | while read i ; do
    dir=$i
    if [[ $i == "drp-community-content" ]] ; then
        dir="content"
    fi
    if [[ $i == "drp-community-contrib" ]] ; then
        dir="contrib"
    fi

    echo "Building bundle: $i"
    ldir=$(pwd)
    if [ -d $dir/content ] ; then
        cd $dir/content
        rm -f ._Version.meta
        drpcli contents bundle $ldir/$i.yaml Version=$version
        cd -
    else
        cd $dir
        rm -f ._Version.meta
        drpcli contents bundle $ldir/$i.yaml Version=$version
        cd -
    fi
    $shasum $i.yaml > $i.sha256

    echo "Building documents: $i"
    drpcli contents document $i.yaml > $i.rst
    echo "Building documents $DOC_VERSION: $i"
    drpcli contents document-md $i.yaml rackn-base-docs/$DOC_VERSION || :
done

