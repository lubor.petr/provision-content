---
Name: haproxy/frontend/map
Description: Automatically configured mapping of frontend service ports
Documentation: |
  # HAProxy Frontend Map

  This parameter stores the automatically discovered frontend port mappings used to generate
  the HAProxy configuration. It defines which ports HAProxy listens on for each service.

  ## Requirements
  - Generated automatically by `haproxy-configure.py`
  - Do not modify manually - use `haproxy/frontend/map-override` for manual configuration

  ## Usage

  The parameter is populated automatically based on discovered services. The structure maps
  service names to their listening ports:

  ```
    service-name: port
    # or for multiple ports:
    service-name: [port1, port2]
  ```

  Example auto-generated configuration:

  ```yaml
    http: 80
    https: [443, 8443]
    postgres: 5432
  ```

Meta:
  color: green
  icon: network
  title: HAProxy Frontend Port Map

Schema:
  type: object
  additionalProperties:
    oneOf:
      - type: integer
      - type: array
        items:
          type: integer
  default: {}
