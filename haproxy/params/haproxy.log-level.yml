---
Name: haproxy/log-level
Description: Logging level for HAProxy configuration script
Documentation: |
  # HAProxy Configuration Script Log Level

  This parameter controls the logging verbosity of the haproxy-configure.py script
  which handles service discovery and configuration generation. Higher log levels
  provide more detailed information for troubleshooting.

  ## Requirements
  - Must be a valid Python logging level
  - Changes take effect on next configuration run

  ## Usage

  Set logging level:
  ```yaml
  haproxy/log-level: INFO
  ```

  Available log levels from least to most verbose:
  - CRITICAL: Only critical errors
  - ERROR: Error conditions
  - WARN: Warning conditions
  - INFO: General operational information (default)
  - DEBUG: Detailed information for troubleshooting
  - NOTSET: All available logging

Meta:
  color: blue
  icon: file text
  title: HAProxy Configuration Log Level

Schema:
  type: string
  default: INFO
  enum:
    - NOTSET
    - DEBUG
    - INFO
    - WARN
    - ERROR
    - CRITICAL
