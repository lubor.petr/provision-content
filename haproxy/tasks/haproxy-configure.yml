---
Name: haproxy-configure
Documentation: |
  # HAProxy Configure Task

  This task handles the configuration and management of HAProxy services. It discovers backend
  services, generates the HAProxy configuration file, and ensures the service is running with
  the correct settings.

  ## Requirements
  - HAProxy must be installed (haproxy-install task)
  - Node must have appropriate permissions to:
    - Read and write /etc/haproxy/haproxy.cfg
    - Manage HAProxy service
    - Configure firewall rules
    - Modify SELinux contexts (if enabled)
  
  ## Templates
  - haproxy-configure.py.tmpl: Main configuration script
  - haproxy-setup-firewalld-and-selinux.sh.tmpl: Security configuration
  - haproxy-restart.sh.tmpl: Service management

  ## Workflow
  1. Discovers backend services using configured filters
  2. Generates HAProxy configuration from templates
  3. Updates firewall and SELinux settings
  4. Validates and applies the configuration
  5. Restarts the HAProxy service

Meta:
  icon: wrench
  color: blue
  title: HAProxy Configuration Task

ExtraClaims:
  - scope: "machines"
    action: "list"
    specific: "*"
  - scope: "templates"
    action: "get"
    specific: "haproxy.cfg.tmpl"

Templates:
  - ID: haproxy-configure.py.tmpl
    Name: haproxy-configure.py
  - ID: haproxy-setup-firewalld-and-selinux.sh.tmpl
    Name: haproxy-setup-firewalld-and-selinux.sh
  - ID: haproxy-restart.sh.tmpl
    Name: haproxy-restart.sh
