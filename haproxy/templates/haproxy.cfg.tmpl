{{- $frontend_map := (.ParamExpand "haproxy/frontend/map") -}}
{{- $frontend_config := (.ParamExpand "haproxy/frontend/config") -}}
{{- $backend_map := (.ParamExpand "haproxy/backend/map") -}}
{{- $backend_config := (.ParamExpand "haproxy/backend/config") -}}

global
  {{- "\n" }}
  {{- range $line := splitList "\n" (.ParamExpand "haproxy/config/global") }}
    {{- if ne $line "" }}
      {{- printf "  %s\n" $line }}
    {{- end -}}
  {{- end -}}

{{- "\n " -}}

defaults
  {{- "\n" }}
  {{- range $line := splitList "\n" (.ParamExpand "haproxy/config/default") }}
    {{- if ne $line "" }}
      {{- printf "  %s\n" $line }}
    {{- end -}}
  {{- end -}}

{{- "\n " -}}

{{- range $svc_name, $svc_ports := $frontend_map }}
{{- $svc_backend_config := get $backend_config $svc_name }}
{{- $svc_frontend_config := get $frontend_config $svc_name }}
### Start config stanzas for {{ $svc_name }}
frontend {{ $svc_name }}-in
  default_backend {{ $svc_name }}_backend
  {{- if kindIs "map" $svc_frontend_config -}}
    {{- with index $svc_frontend_config "mode" }}{{ if . }}{{ printf "\n  mode %s" . }}{{ end }}{{ end -}}
    {{- if $this_config := get $svc_frontend_config "config" -}}
      {{- "\n" -}}
        {{- range $line := splitList "\n" $this_config -}}
          {{- if ne $line "" -}}
            {{- printf "  %s\n" $line -}}
          {{- end -}}
        {{- end -}}
    {{- end -}}
  {{- end -}}
  {{- if kindIs "slice" $svc_ports -}}
    {{- range $svc_port := $svc_ports -}}
      {{ printf "\n  bind *:%d" (int $svc_port) -}}
    {{- end -}}
  {{- else -}}
    {{ printf "\n  bind *:%d" (int $svc_ports) -}}
  {{- end }}

backend {{ $svc_name }}_backend
  {{- /* get the "config:" bits out of the backend services map */ -}}
  {{- if kindIs "map" $svc_backend_config -}}
    {{- with index $svc_backend_config "mode" }}{{ if . }}{{ printf "\n  mode %s" . }}{{ end }}{{ end -}}
    {{- with index $svc_backend_config "balance" }}{{ if . }}{{ printf "\n  balance %s" . }}{{ end }}{{ end -}}
    {{- if $this_config := get $svc_backend_config "config" -}}
    {{- "\n" -}}
      {{- range $line := splitList "\n" $this_config }}
        {{- if ne $line "" }}
          {{- printf "  %s\n" $line }}
        {{- end -}}
      {{- end -}}
    {{- end -}}
  {{- end }}
  {{- range $backend_node := get $backend_map $svc_name }}
  {{ $backend_node }}
  {{- end -}}
  {{- "\n " -}}
{{- end }}
