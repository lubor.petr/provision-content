# Apache

## Introduction

Apache Web Server is a widely used and recognized software that serves as a common starting point for many when setting up a web server due to its ease of setup and extensive community support. This content bundle leverages the familiarity of Apache to help users learn about the RackN DRP platform. By demonstrating multiple methods to accomplish tasks using Apache, users can gain a better understanding of how to utilize the DRP platform effectively.

## Technical Requirements

- Digital Rebar Provision environment
- Network access to package repositories
- Supported operating system:
  - Alma Linux 9 (recommended)
  - VMware Photon OS 5
- Root/sudo access for installation
- Storage space for OS installation
- Network connectivity for package installation

## Prerequisites

The following content bundles must be available in your DRP environment:
- drp-community-content
- universal (version >=4.13.0)
- haproxy (version >=0.0.0)

## Architecture

The Apache content bundle consists of several key components that work together:

1. **Installation Parameters**
   - `apache/base-install-os`: Defines OS installation pipeline
   - `apache/packages`: Maps package names across platforms
   - `apache/template`: Configures default index page template

2. **Tasks**
   - `apache-install`: Handles package installation and firewall configuration
   - `apache-configure`: Manages Apache configuration and index page setup

3. **Profiles**
   - `apache-web-server`: Basic profile for single-server deployment
   - `universal-application-apache-web-server`: Complete pipeline for infrastructure deployment

## Installation

To install the `apache` content bundle, use the following commands:

```bash
git clone https://gitlab.com/rackn/provision-content
cd provision-content/apache
drpcli contents bundle ../apache.yml
drpcli contents upload ../apache.yml
```

## Execution

There are two methods for executing this content bundle:

### Method 1: Profile Assignment

Use this method for single server deployments:

```bash
# Assign the profile to a machine
drpcli machines addprofile "machine-uuid" "apache-web-server"

# Execute the universal-runbook workflow
drpcli machines workflow "machine-uuid" "universal-runbook"
```

### Method 2: Universal Application Pipeline

Use this method for cluster deployments:

```bash
# Assign the pipeline profile
drpcli machines addprofile "machine-uuid" "universal-application-apache-web-server"

# Execute the workflow
drpcli machines workflow "machine-uuid" "universal-linux-install"
```

### Choosing an Option

The method you choose depends on your specific use case:

1) **Profile Assignment**: Ideal for single-machine deployments, testing, or development environments. This method provides more flexibility for customization and is simpler to manage for individual machines.

2) **Universal Application Pipeline**: Best suited for production environments where you need to deploy multiple Apache nodes as part of a larger infrastructure. This method ensures consistency across deployments and integrates well with other infrastructure components like load balancers.

## Troubleshooting

Common issues and solutions:

1. **Package Installation Failures**
   - Verify network connectivity to package repositories
   - Ensure correct OS is selected in `apache/base-install-os`
   - Check system meets storage requirements

2. **Firewall Configuration**
   - Verify port 80 is open using `iptables -L`
   - Check iptables service is running
   - Ensure firewall rules are persisted

3. **Apache Service Issues**
   - Check service status: `systemctl status httpd`
   - Review logs: `/var/log/httpd/error_log`
   - Verify permissions on web root directory

## Talk to Us!

The [RackN Team](https://rackn.com) uses Slack to communicate with our
[Digital Rebar Provision](http://rebar.digital) community. If you haven't already
signed up for our Community Slack, you can do so at:

  * https://rackn.com/support/slack/

We look forward to helping you with your provisioning automation journey towards
a fully self sustained Continuously Delivered Data Center journey.
