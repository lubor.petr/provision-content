---
Name: sledgehammer-create-stage1
Description: Create the filesystem for the stage 1 initramfs.
Documentation: |
  We need the stage1 image to be as small as we can get it, and
  still be possible to load the stage2 image.  That means
  that the stage1 image has little more than busybox, a few required
  modules, and a swuashfs that contains only modules that may be required
  for fetching the stage2 image.
Templates:
  - Name: prepare-stage-1
    Contents: |
      #!/bin/bash
      set -x
      set -e
      ARCH={{.Machine.Arch}}
      bk="$(cd /boot && echo config-*)"
      bk="${bk#config-}"
      KERNEL="vmlinuz-${bk}"
      cd /IMAGE
      echo "Staging bootloader, kernel and busybox for stage1 initramfs"
      cp /boot/$KERNEL /IMAGE/vmlinuz0
      {{if (eq .Machine.Arch "amd64")}}
      cp /boot/efi/EFI/almalinux/grubx64.efi /IMAGE/grubx64.efi
      cp /boot/efi/EFI/almalinux/shimx64.efi /IMAGE/shimx64.efi
      {{else if (eq .Machine.Arch "arm64")}}
      cp /boot/efi/EFI/almalinux/grubaa64.efi /IMAGE/grubaa64.efi
      cp /boot/efi/EFI/almalinux/shimaa64.efi /IMAGE/shimaa64.efi
      {{end}}
      # Make module archive, trimming out things not needed to netboot.
      [[ -d lib ]] && rm -rf lib
      mkdir -p lib/modules
      echo "Copying over required modules and firmware"
      cp -a "/lib/modules/$bk" lib/modules
      cd "lib/modules/$bk/kernel"
      rm -rf drivers/media \
          drivers/ata \
          drivers/bluetooth \
          drivers/cdrom \
          drivers/firewire \
          drivers/gpu \
          drivers/hid \
          drivers/hwmon \
          drivers/input/misc \
          drivers/isdn \
          drivers/md \
          drivers/mfd \
          drivers/mmc \
          drivers/net/can \
          drivers/net/wireless \
          drivers/nvme \
          drivers/parport \
          drivers/pcmcia \
          drivers/scsi \
          drivers/uio \
          drivers/usb/atm \
          drivers/usb/image \
          drivers/usb/misc \
          drivers/usb/serial \
          drivers/usb/storage \
          drivers/uwb \
          fs/btrfs \
          fs/ceph \
          fs/cifs \
          fs/cramfs \
          fs/dlm \
          fs/exofs \
          fs/ext4 \
          fs/fat \
          fs/fuse \
          fs/gfs2 \
          fs/isofs \
          fs/jbd2 \
          fs/nfs \
          fs/nfs_common \
          fs/nfsd \
          fs/udf \
          fs/xfs \
          net/6lowpan \
          net/bluetooth \
          net/bridge \
          net/can \
          net/ceph \
          net/openvswitch \
          net/wireless \
          sound
      cd /IMAGE
      depmod -b $PWD -a "$bk"
      # Take only the firmware our modules will need.
      while read -r mod; do
          while read -r fw; do
              targetDir=lib/firmware
              if [[ ${fw%/*} != $fw ]]; then
                  targetDir="$targetDir/${fw%/*}"
              fi
              mkdir -p "$targetDir"
              cp -t "$targetDir" "/lib/firmware/$fw"* || :
          done < <(modinfo -F firmware "$mod")
      done < <(find lib/modules -type f -name '*.ko*')
      echo "Making module/firmware archive"
      find lib |sort |cpio -o -R 0:0 --format=newc |xz -T0 -c >initrd/lib.cpio.xz
      rm -rf lib
      cd /IMAGE/initrd
      mkdir -p dev proc sys bin sbin lib lib64 etc usr/share/udhcpc tmp newinitramfs
      mv /sbin/busybox bin/busybox
      (cd bin && ./busybox --install . && rm modprobe insmod ip)
      chmod 755 init usr/share/udhcpc/default.script
      # Pull in working modprobe/depmod/etc
      copy_dyn() {
         [[ -f $PWD/$1 ]] && return
         mkdir -p "$PWD/$(dirname $1)"
         cp "$1" "$PWD/$1"
         local a b c rest
         while read a b c rest; do
              if [[ $a = */ld-linux* ]]; then
                  copy_dyn "$a"
                  continue
              fi
              [[ $b = '=>' ]] || continue
              [[ -f $PWD/$c ]] && continue
              copy_dyn "$c"
          done < <(ldd "$1")
      }
      for f in /sbin/modprobe /sbin/insmod /sbin/ip; do
          copy_dyn "$f"
      done
      {{if (eq .Machine.Arch "amd64")}}
      rpm -e grub2-efi-x64 shim-x64
      {{end}}
